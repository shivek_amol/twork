<?php
include 'config/DbHandler.php';
include 'config/DbHandler_Update.php';
include 'config/DbHandler_Select.php'; 

require 'Slim/Slim.php';

global $app;

// Public $app;

\Slim\Slim::registerAutoloader();
// $app = new \Slim\Slim();

$logWriter = new \Slim\LogWriter(fopen('log.txt', 'a'));
$app = new \Slim\Slim(array('log.writer' => $logWriter));
$logger = $app->log;
$logger->setEnabled(true);
$logger->setLevel(\Slim\Log::DEBUG);


$app->get('/users','getUsers');
// $app->get('/updates','getUserUpdates');
// $app->post('/updates', 'insertUpdate');
// $app->delete('/updates/delete/:update_id','deleteUpdate');
// $app->get('/users/search/:query','getUserSearch');



function getUsers() {

	echo "Success" ;
}


function hello_user(){

	echo "Hi User ";



}


$app->post('/hi', 'hello_user');


function insertUpdate() 
{
	error_reporting(0);
	

	global $logger;
	global $app;

	$app = new \Slim\Slim();
	$app->response->headers->set('Content-Type', 'application/json');
	$data = $app->request->getBody();

	$DbHandler = new DbHandler();
	$DbHandler->get_client_table();

	print_r($data);
	echo "Success";
}





/* function insert user data start here*/

$app->post('/insert_user', 'insert_user_detail');

function insert_user_detail()
{
	global $logger;
	global $app;
	$app->response->headers->set('Content-Type', 'multipart/form-data');

	// $app->response->headers->set('Content-Type', 'application/json');
	$data = $app->request->getBody();



	 // print_r($data);

// print_r($_FILES);


// foreach($_FILES["myFile"]["tmp_name"] as $key=>$tmp_name)
// {


	$target_dir = "files/";
$target_file = $target_dir . basename($_FILES["myFile"]["name"]); // filepath
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image


if (move_uploaded_file($_FILES["myFile"]["tmp_name"], $target_file)) 
{
        // echo "The file ". basename( $_FILES["myFile"]["name"]). " has been uploaded.";
}



//}
	 // print_r($_SERVER['HTTP_JSONOBJECT']);

if (isset($_SERVER['HTTP_JSONOBJECT'])) 
{
	$data = json_decode($_SERVER['HTTP_JSONOBJECT'],true);
	
}else{

// echo $data;

	$data = json_decode($data,true);
}


//print_r($data);
// echo $target_file;
$mydecode_file = base64_decode($data['myFile']); 
$im = imagecreatefromstring($mydecode_file); 
if ($im !== false) 
{
	$fileName = date('YmdHis') .".png";
	$resp = imagepng($im,$target_dir.$fileName);
	$target_file = $target_dir.$fileName;


	imagedestroy($im);
} else {
	// echo 'An error occurred.'; 
}


	$source_img = $target_file;
	$destination_img =  $target_dir.date('YmdHis') ."_compress.png";

//   'files/20160917173135_compress.png';

$target_file = compress($source_img, $destination_img, 15);
// echo $target_file;

		 // print_r($data);

try {
	$DbHandler = new DbHandler();
	$insert_list = $DbHandler->insert_user_data($data,$target_file);

	if ($insert_list)
	{
		$response['status'] = Constants::getSuccess();
		$response['user_id'] = $insert_list['user_id'];
		$response['employer_id'] = $insert_list['employer_id'];
		$response['employee_id'] = $insert_list['employee_id'];


		$response['msg'] = "Data inserted successfully.";
		echoResponse(200, $response);
	}else{
		$response['status'] = Constants::getError();
		$response['error_msg'] = "Request get failed or requested data not found.";
		echoResponse(Constants::getError(), $response);
	}


} catch(Exception $e)
{
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
	$response['status'] = Constants::getError();
	$response['error'] = $e->getMessage();
	echoResponse(Constants::getError(), $response);

	 	// echo '{"error":{"text":'. $e->getMessage() .'}}'; 
}


}





/* image compression function start here */

function compress($source, $destination, $quality) 
{

	$info = getimagesize($source);

	if ($info['mime'] == 'image/jpeg') 
		$image = imagecreatefromjpeg($source);

	elseif ($info['mime'] == 'image/gif') 
		$image = imagecreatefromgif($source);

	elseif ($info['mime'] == 'image/png') 
		$image = imagecreatefrompng($source);

	imagejpeg($image, $destination, $quality);

	return $destination;
}

/* image compression function end here*/











/* function end here*/

$app->post('/UserProfile', 'getUserProfile');

function getUserProfile()
{


	global $logger;
	global $app;

	$app = new \Slim\Slim();
	$app->response->headers->set('Content-Type', 'application/json');
	$data = $app->request->getBody();

// echo $data;

	$data = json_decode($data,true);

	
	try {

		$DbHandler = new DbHandler();
		$user_profile = $DbHandler->getUserProfileData($data);
		// print_r($user_profile);
		if (count($user_profile))
		{
			$user_profile['status'] = Constants::getSuccess();
			// $response['user_profile'] = $user_profile;
			// $response['msg'] = "Data inserted successfully.";
			echoResponse(200, $user_profile);
		}else{
			$response['status'] = Constants::getError();
			$response['error_msg'] = "Request get failed or requested data not found.";
			echoResponse(Constants::getError(), $response);
		}


	} catch(Exception $e)
	{
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		$response['status'] = Constants::getError();
		$response['error'] = $e->getMessage();
		echoResponse(Constants::getError(), $response);

	 	// echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}




}  // end 


/* get user profile service start here*/


$app->post('/userProfileImage', 'user_profile_image');

function user_profile_image()
{
	global $logger;
	global $app;

	$app = new \Slim\Slim();
	$app->response->headers->set('Content-Type', 'application/json');
	$data_payload = $app->request->getBody();

	$target_dir = "files/";
	$data_payload = json_decode($data_payload,true);

	$img_list = array();

	$img_list['user_id'] = $data_payload['user_id'];

	print_r($data_payload);

	try {

		foreach ($data_payload['myImages']  as $key => $byte_image) 
		{ // for llop start here

			// $img_list['img'][$key] = $target_dir.$byte_image;


			$mydecode_file = base64_decode($byte_image); 
			$im = imagecreatefromstring($mydecode_file); 
			if ($im !== false) 
			{
				$fileName = date('YmdHis') .".png";
				$resp = imagepng($im,$target_dir.$fileName);

				$img_list['img'][$key] = $target_dir.$fileName;
				imagedestroy($im);
			} else {
				// echo 'An error occurred.'; 
			}



			$source_img = $img_list['img'][$key];
			$destination_img =  $target_dir.date('YmdHis') ."_compress.png";

//   'files/20160917173135_compress.png';

			$img_list['img'][$key] = compress($source_img, $destination_img, 15);

		}  // loop end here





		$DbHandler_Update = new DbHandler_Update();
		$profile_img = $DbHandler_Update->update_user_profile_image($img_list);
		// print_r($user_profile);
		if ($profile_img)
		{
			$user_profile['status'] = Constants::getSuccess();
			$user_profile['msg'] = "Images inserted successfully.";
			echoResponse(200, $user_profile);
		}else{
			$response['status'] = Constants::getError();
			$response['error_msg'] = "Request get failed or requested data not found.";
			echoResponse(Constants::getError(), $response);
		}


	} catch(Exception $e)
	{
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		$response['status'] = Constants::getError();
		$response['error'] = $e->getMessage();
		echoResponse(Constants::getError(), $response);

	 	// echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}

}



/* service end here*/



/* getprofiledetail function start here */


$app->post('/getJobDetail', 'get_Job_Detail');

function get_Job_Detail()
{

	global $logger;
	global $app;
	$app->response->headers->set('Content-Type', 'application/json');
	$data_payload = $app->request->getBody();

	try{

		$ids = json_decode($data_payload,true);
		$DbHandler_Select  = new DbHandler_Select();
		$getList = $DbHandler_Select->select_user_profile($ids);





if (count($getList))
		{
			$user_profile['status'] = Constants::getSuccess();
			$user_profile['job_details'] = $getList;
			echoResponse(200, $user_profile);
		}else{
			$response['status'] = Constants::getError();
			$response['error_msg'] = "Request get failed or requested data not found.";
			echoResponse(Constants::getError(), $response);
		}


	}catch(Exception $e)
	{
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		$response['status'] = Constants::getError();
		$response['error'] = $e->getMessage();
		echoResponse(Constants::getError(), $response);

	 	// echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}




}

/* getprofiledetail function end here */












// /*start of category list */

function getCategory_list() 
{
	global $logger;
	global $app;
	$app->response->headers->set('Content-Type', 'application/json');
	// $data = $app->request->getBody();
	try {
		$DbHandler = new DbHandler();
		$category_list = $DbHandler->get_category_list();
		// echo '{"users": ' . json_encode($users) . '}';
		// print_r($category_list);
 		// echo json_encode($category_list);

		// print_r($category_list);

		if ($category_list)
		{
			$response['status'] = Constants::getSuccess();
			$response['data'] = $category_list;
			echoResponse(200, $response);
		}else{
			$response['status'] = Constants::getError();
			$response['error_msg'] = "Request get failed or requested data not found.";
			echoResponse(Constants::getError(), $response);
		}


	} catch(Exception $e)
	{
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		$response['status'] = Constants::getError();
		$response['error'] = $e->getMessage();
		echoResponse(Constants::getError(), $response);

	 	// echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}  
$app->post('/getCategory', 'getCategory_list');


// /* end of category list */


/* sub category function start here*/

function getSubCategory_list(){
	global $app;
	$app->response->headers->set('Content-Type', 'application/json');
	$categoryJason = $app->request->getBody();
	try {
		$DbHandler = new DbHandler();
		$sub_categorylist = $DbHandler->get_sub_category_list($categoryJason);



		// if ($sub_categorylist) {
		$response['status'] = Constants::getSuccess();
		$response['sub_cat_list'] = $sub_categorylist;
		echoResponse(200, $response);
		// } else {
		// 	$response['status'] = Constants::getError();
		// 	$response['error'] ="Request get failed or requested data not found.";
		// 	echoResponse(Constants::getError(), $response);
		// }

	} catch(Exception $e)
	{
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		$response['status'] = Constants::getError();
		$response['error'] = $e->getMessage();
		echoResponse(Constants::getError(), $response);

	 	// echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}


}

$app->post('/getSubCategory', 'getSubCategory_list');

/* dub category function enf here*/


function echoResponse($status_code, $response) 
{
	$app = \Slim\Slim::getInstance();
	// Http response code
	$app->status($status_code);

//    $response['Content-Type'] = 'application/json';
	$app->contentType('application/json;charset=utf-8');

	echo json_encode($response);
}

$app->run();
?>