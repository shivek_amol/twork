<?php
require "../notorm/NotORM.php";
require '../slim/libs/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$logWriter = new \Slim\LogWriter(fopen('log.txt', 'a'));
$app = new \Slim\Slim(array('log.writer' => $logWriter));
$logger = $app->log;
$logger->setEnabled(true);
$logger->setLevel(\Slim\Log::DEBUG);
// $dsn = "mysql:dbname=cageread_crc_db;host=localhost";
// $username = "cageread";
// $password = "ns6WiL29d8";
$dsn = "mysql:dbname=cageread_crc_db_new;host=localhost";
$username = "root";
$password = "root";
$pdo = new PDO($dsn, $username, $password);
$notOrm = new NotORM($pdo);

require_once 'app/routes/Event.php';
require_once 'app/routes/Login.php';
require_once 'app/routes/Registration.php';
require_once 'app/routes/EventParticipant.php';
require_once 'app/routes/EventFighter.php';
require_once 'app/routes/Experiment.php';
require_once 'app/routes/Fighter.php';
require_once 'app/routes/User.php';
require_once 'app/routes/Promoter.php';
require_once 'app/routes/PushNotification.php';
require_once 'app/routes/iOSPurchase.php';
/**
 * @apiDefine MySuccess
 * @apiSuccess {string} firstname The users firstname.
 * @apiSuccess {number} age The users age.
 */

/**
 * @apiDefineStructure credential
 *
 * @apiParam {Object} credential Users credential
 * @apiParam {String} credential.email Users unique email.
 * @apiParam {String} credential.pass Users password
 */
$app->get('/hello/:name', function ($name) {
	echo "Hello, $name";
});

//$app->add(new \Slim\Middleware\ContentTypes());

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoResponse($status_code, $response) {
	$app = \Slim\Slim::getInstance();
	// Http response code
	$app->status($status_code);

//    $response['Content-Type'] = 'application/json';
	//    $app->contentType('application/json;charset=utf-8');

	echo json_encode($response);
}

$app->run();
?>
