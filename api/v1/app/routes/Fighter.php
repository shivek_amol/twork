<?php
require_once 'Models/UserModel.php';

/* $app->post('/fighter/:id','fighterForId'); */
/* $app->post('/fighter','getFighterFor'); */

/**
 * @apiDefine FighterParams
 * @apiParam {Object[]} user
 * @apiParam {Number} user.user_id User id
 * @apiParam {String} user.first_name First Name of the user
 * @apiParam {String} user.last_name Last name
 * @apiParam {String} user.email Email id
 * @apiParam {Number} user.user_type
 * @apiParam {String} user.gender
 * @apiParam {String} user.city
 * @apiParam {String} user.state
 * @apiParam {Object} user.type_info Only if the user loggin is Fighter
 * @apiParam {Number} user.type_info.fighter_id Fighter id
 * @apiParam {Number} user.type_info.user_id
 * @apiParam {Number} user.type_info.weight_class_id Weight class of the fighter
 * @apiParam {Number} user.type_info.mma 1 if fighter fights mma 0 if not
 * @apiParam {Number} user.type_info.boxing if fighter fights boxing 0 if not
 * @apiParam {Number} user.type_info.kickboxing if fighter fights kickboxing 0 if not
 *
 *
 */

/**
 * @api {post} /fighter Getfighter Details
 * @apiName GetFighterDetail
 * @apiGroup FighterParams
 *
 * @apiUse Credential
 *
 * @apiParam {Object} event_fighter Event Fighter
 * @apiParam {Number} event_fighter.event_id Id of the event in which participating
 * @apiParam {Number} event_fighter.user_agent 1 for Android 0 for iOS
 *
 *
 * @apiSuccess {String} message Message
 * @apiSuccess {Number} status  200
 */
$app->post('/fighter', 'fighterForId');
$app->post('/update_fighter', 'updateFighter');
$app->post('/get_fighter', 'getFighters');

/* function getFighterFor(){ */
/* 	global $notOrm; */
/* 	global $app; */
/* 	$app->contentType('application/json'); */
/*     $body = $app->request->getBody(); */
/*     $requestParams = json_decode($body, true); */
/*     $credentialPresent = verifyRequiredCredentials($requestParams); */

/*     if ($credentialPresent) { */
/*         $email = $requestParams['credential']['email']; */
/*         $pass = $requestParams['credential']['pass']; */
/*         $userObject = new UserModel($notOrm); */
/*         $user = $userObject->authenticateUser($email , $pass); */

/*         /1* $user = $notOrm->user()-> *1/ */
/*         /1*                 select(implode(',', Constant::$user_projection))-> *1/ */
/*         /1*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); *1/ */
/*         if ($user && $user['user_type'] == USER_TYPE_FIGHTER) { */
/* 	        $response = getFighter($notOrm, $user); */
/*             unset($response['message']); */
/*             unset($response['status']); */
/* 	        echoResponse(200, $response); */
/*         }else{ */
/*         	echoResponse(200,array( */
/* 	            "status" => 400, */
/* 	            "message" => "Invalid credential for fighter" */
/* 	        )); */
/*         } */
/*     }else{ */
/*     	echoResponse(200,array( */
/*             "status" => 400, */
/*             "message" => "Invalid Params" */
/*         )); */
/*     } */
/* } */

/* function fighterForId($id){ */
function fighterForId() {
	global $pdo;
	global $notOrm;
	global $app;
	$app->contentType('application/json');
	$body = $app->request->getBody();
	$requestParams = json_decode($body, true);
	$credentialPresent = verifyRequiredCredentials($requestParams);
	$filterPresent = isset($requestParams['filter']['user_id']) && $requestParams['filter']['user_id'] > 0;

	if ($credentialPresent && $filterPresent) {
		$email = $requestParams['credential']['email'];
		$pass = $requestParams['credential']['pass'];
		$id = $requestParams['filter']['user_id'];

		$userObject = new UserModel($notOrm);
		$user = $userObject->authenticateUser($email, $pass);

		/* $user = $notOrm->user()-> */
		/*                 select(implode(',', Constant::$user_projection))-> */
		/*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); */
		if ($user) {
			$requestedFighter = $notOrm->user()->
			select(implode(',', Constant::$user_projection))->
			where("id = ? AND user_type = ?", $id, USER_TYPE_FIGHTER)->fetch();
			if ($requestedFighter && $requestedFighter['user_type'] == USER_TYPE_FIGHTER) {
				// $response = getFighter($notOrm, $requestedFighter);
				$response = getFighter($pdo, $requestedFighter);
				unset($response['message']);
				unset($response['status']);
				echoResponse(200, $response);
			} else {
				echoResponse(200, array(
					"status" => 400,
					"message" => "Requested fighter does not exists",
				));
			}
		} else {
			echoResponse(200, array(
				"status" => 400,
				"message" => "Invalid credential",
			));
		}
	} else {
		echoResponse(200, array(
			"status" => 400,
			"message" => "Invalid Params",
		));
	}
}

function updateFighter() {
	global $logger;
	global $notOrm;
	global $app;

	$logger->info("update Fighter");
	$app->contentType('application/json');
	$body = $app->request->getBody();
	$status = new Constant();
	$requestParams = json_decode($body, true);

	$paramsPresent = verifyUserParams($requestParams['user']);

	if (!$paramsPresent) {
		$logger->info('Invalid Params');
		$response['status'] = 400;
		$response['message'] = "Invalid Params";
		echoResponse(200, $response);
		return;
	}

	$fighterParamsPresent = verifyFighterParams($requestParams['user']['type_info']);

	/* $emailAvailable = isEmailAvailable($notOrm, $requestParams['user']['email']); */

	/* if(!$emailAvailable){ */
	/*     $logger->info('Email already taken'); */
	/*     $response['status'] = 400; */
	/*     $response['message'] = "Please user another email address"; */
	/*     echoResponse(200,$response); */
	/*     return; */
	/* } */

	$credentialPresent = verifyRequiredCredentials($requestParams);

	if ($credentialPresent) {
		$logger->info('Credentials Present');
		$email = $requestParams['credential']['email'];
		$pass = $requestParams['credential']['pass'];
		$userObject = new UserModel($notOrm);
		$user = $userObject->authenticateUser($email, $pass);

		/* $user = $notOrm->user()-> */
		/*     select(implode(',', Constant::$user_projection))-> */
		/*     where("email = ? AND pass = ?", $email, $pass)->fetch(); */

		if ($user && $user['id'] == $requestParams['user']['id']) {
			$logger->info('params are valid');

			/* $userParam['user_id'] = $requestParams['user']['user_id']; */
			/* $userParam['email'] = $requestParams['user']['email']; */
			/* $userParam['user_agent'] = $requestParams['user']['user_agent']; */
			$userParam['first_name'] = $requestParams['user']['first_name'];
			$userParam['last_name'] = $requestParams['user']['last_name'];
			$userParam['pass'] = md5($requestParams['user']['pass']);
			$userParam['user_type'] = $requestParams['user']['user_type'];
			$userParam['gender'] = $requestParams['user']['gender'];
			$userParam['city'] = $requestParams['user']['city'];
			$userParam['state'] = $requestParams['user']['state'];
			$userParam['push_notification_id'] = $requestParams['user']['push_notification_id'];
			$userParam['update_time'] = date("Y-m-d H:i:s");

			$typeInfo = $requestParams['user']['type_info'];

			$fighterParams['weight_class_id'] = $typeInfo['weight_class_id'];
			$fighterParams['fight_team'] = $typeInfo['fight_team'];
			$fighterParams['update_time'] = date("Y-m-d H:i:s");
			$fighterParams['mma'] = $typeInfo['mma'];
			$fighterParams['boxing'] = $typeInfo['boxing'];
			$fighterParams['kickboxing'] = $typeInfo['kickboxing'];
			$fighterParams['level'] = $typeInfo['level'];

			$notOrm->transaction = "BEGIN";
			/* $user = $notOrm->user()->insert($userParam); */
			/* $user = $notOrm->user()->insert_update(array('id'=>$useParam['user_id']), $userParam, $userParam); */

			$updatedUser = $notOrm->user()->where('id', $user['id'])->fetch();

			$updatedUser->update($userParam);
			/* $fighterParams['user_id'] = $user['id']; */
			$updatedFighter = $notOrm->fighter()->where('user_id', $user['id'])->fetch();
			$updatedFighter->update($fighterParams);
			/* $fighter = $notOrm->fighter()->insert_update(array('id' => $fighterParams['user_id']), */
			/* $fighterParams, $fighterParams); */

			$recordsParams = array();

			foreach ($typeInfo['record'] as $record) {
				$logger->info('win');
				$logger->info($record['win']);
				$logger->info('loss');
				$logger->info($record['loss']);
				$logger->info('draw');
				$logger->info($record['draw']);
				$logger->info('fight_type_id');
				$logger->info($record['fight_type_id']);
				$recordsParams[] = array(
					/* 'fighter_id' => $fighter['id'], */
					'win' => $record['win'],
					'loss' => $record['loss'],
					'draw' => $record['draw'],
					'fight_type_id' => $record['fight_type_id']);
				/* echo "Fighter id".$updatedFighter['id']; */
				/* echo "fight_type_id".$record['fight_type_id']; */
				$logger->info('fighter id');
				$logger->info($updatedFighter['id']);

				$updatedRecord = $notOrm->record()->
				where('fighter_id = ? AND fight_type_id = ?',
					$updatedFighter['id'], $record['fight_type_id'])->fetch();
				if ($updatedRecord) {
					unset($record['id']);
					$record['fighter_id'] = $updatedFighter['id'];
					$updatedRecord->update($record);
				} else {
					unset($record['id']);
					$record['fighter_id'] = $updatedFighter['id'];
					$updatedRecord = $notOrm->record()->insert($record);
					$updatedRecord = $notOrm->record()->where('id', $updatedRecord['id']);
				}
				$updatedRecord = iterator_to_array($updatedRecord);
				$records[] = $updatedRecord;
			}

			if ($fighterParams['mma'] == 0) {
				$mmaRecord = $notOrm->record()->where('fighter_id = ? AND fight_type_id = ?',
					$updatedFighter['id'], FIGHT_TYPE_MMA)->fetch();
				if ($mmaRecord) {
					$mmaRecord->delete();
				}
			}
			if ($fighterParams['boxing'] == 0) {
				$boxingRecord = $notOrm->record()->where('fighter_id = ? AND fight_type_id = ?',
					$updatedFighter['id'], FIGHT_TYPE_BOXING)->fetch();
				if ($boxingRecord) {
					$boxingRecord->delete();
				}
			}
			if ($fighterParams['kickboxing'] == 0) {
				$kickboxingRecord = $notOrm->record()->where('fighter_id = ? AND fight_type_id = ?',
					$updatedFighter['id'], FIGHT_TYPE_KICKBOXING)->fetch();
				if ($kickboxingRecord) {
					$kickboxingRecord->delete();
				}
			}

			/* $records = call_user_func_array('$notOrm->record()->insert',$recordsParams); */

			$updatedUser = $notOrm->user()->where('id', $user['id'])->fetch();
			$updatedFighter = $notOrm->fighter()->where('user_id', $user['id'])->fetch();

			$notOrm->transaction = "COMMIT";

			$fighterArr = iterator_to_array($updatedFighter);

			$fighterArr['record'] = array_values($records);

			$response['status'] = 200;
			$response['message'] = 'Fighter updated sucessfully';
			$user = iterator_to_array($user);
			/* $response['user'] = array($user, */
			$response['user'] = array(
				'type_info' => $fighterArr);
			$response['user'] = array_merge($response['user'], $user);

			echoResponse(200, $response);

		} else {
			//not permitted
			$logger->info('not permitted');
			$response['status'] = 400;
			$response['message'] = 'Not permitted';
			echoResponse(200, $response);
		}
	} else {
		//invalid params
		$logger->info('invalid params');
		$response['status'] = 400;
		$response['message'] = 'Invalid Params';
		echoResponse(200, $response);
	}
}

function getFighters() {
	global $logger;
	global $notOrm;
	global $app;
	global $pdo;
	$logger->info("get fighters");
	$app->contentType('application/json');
	$body = $app->request->getBody();
	$status = new Constant();
	$requestParams = json_decode($body, true);

	$credentialPresent = verifyRequiredCredentials($requestParams);
	if ($credentialPresent) {
		$logger->info('Credential Present');
		$email = $requestParams['credential']['email'];
		$pass = $requestParams['credential']['pass'];
		$userObject = new UserModel($notOrm);
		$user = $userObject->authenticateUser($email, $pass);

		/* $user = $notOrm->user()-> */
		/*                 select(implode(',', Constant::$user_projection))-> */
		/*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); */
		/* echo 'User: '.$user['user_type']; */
		if ($user && $user['user_type'] == USER_TYPE_PROMOTER) {
			$logger->info('Access granted');

			$sqlQuery = null;
			$queryParams = array();
			if (isset($requestParams['filter']['state']) && $requestParams['filter']['state'] > 0) {
				$queryParams[':state'] = $requestParams['filter']['state'];
				$sqlQuery = ' WHERE U.state = :state ';
			}

			if (isset($requestParams['filter']['weight_class_id'])
				&& $requestParams['filter']['weight_class_id'] > 0) {
				if (is_null($sqlQuery)) {
					$queryParams[':weight_class_id'] = $requestParams['filter']['weight_class_id'];
					$sqlQuery = ' WHERE F.weight_class_id = :weight_class_id';
				} else {
					$queryParams[':weight_class_id'] = $requestParams['filter']['weight_class_id'];
					$sqlQuery = $sqlQuery . ' AND F.weight_class_id = :weight_class_id';
				}
			}

			if (isset($requestParams['filter']['city'])
				&& $requestParams['filter']['city'] > 0) {
				if (is_null($sqlQuery)) {
					$queryParams[':city'] = $requestParams['filter']['city'];
					$sqlQuery = ' WHERE U.city = :city';
				} else {
					$queryParams[':city'] = $requestParams['filter']['city'];
					$sqlQuery = $sqlQuery . ' AND U.city = :city';
				}
			}

			if (isset($requestParams['filter']['mma'])
				&& $requestParams['filter']['mma'] > 0) {
				if (is_null($sqlQuery)) {
					$queryParams[':mma'] = $requestParams['filter']['mma'];
					$sqlQuery = ' WHERE F.mma = :mma';
				} else {
					$queryParams[':mma'] = $requestParams['filter']['mma'];
					$sqlQuery = $sqlQuery . ' AND F.mma = :mma';
				}
			}

			if (isset($requestParams['filter']['boxing'])
				&& $requestParams['filter']['boxing'] > 0) {
				if (is_null($sqlQuery)) {
					$queryParams[':boxing'] = $requestParams['filter']['boxing'];
					$sqlQuery = ' WHERE F.boxing = :boxing';
				} else {
					$queryParams[':boxing'] = $requestParams['filter']['boxing'];
					$sqlQuery = $sqlQuery . ' AND F.boxing = :boxing';
				}
			}
			if (isset($requestParams['filter']['kickboxing'])
				&& $requestParams['filter']['kickboxing'] > 0) {
				if (is_null($sqlQuery)) {
					$queryParams[':kickboxing'] = $requestParams['filter']['kickboxing'];
					$sqlQuery = ' WHERE F.kickboxing = :kickboxing';
				} else {
					$queryParams[':kickboxing'] = $requestParams['filter']['kickboxing'];
					$sqlQuery = $sqlQuery . ' AND F.kickboxing = :kickboxing';
				}
			}

			/* $sql ="SELECT U.id,U.first_name,U.last_name,U.email,U.pass,U.user_type,U.gender,U.city,U.state, */
			/*     U.push_notification_id,U.user_agent,U.update_time,U.del_flag, */
			/*     F.id AS fid,F.user_id,F.weight_class_id,F.fight_team,F.update_time,F.mma,F.boxing,F.kickboxing,F.level, */
			/*     R.id AS rid,R.fighter_id,R.fight_type_id,R.draw,R.win,R.loss */
			/*     FROM user AS U */
			/*     INNER JOIN fighter AS F ON U.id = F.user_id */
			/*     LEFT JOIN record AS R ON U.id = R.fighter_id"; */

			$sql = "SELECT U.id,U.first_name,U.last_name,U.email,U.pass,U.user_type,U.gender,U.city,U.state,
                U.push_notification_id,U.user_agent,U.update_time,U.del_flag,
                F.id AS fid,F.user_id,F.weight_class_id,F.fight_team,F.update_time,F.mma,F.boxing,F.kickboxing,F.level,
                R.id AS rid,R.fighter_id,R.fight_type_id,R.draw,R.win,R.loss,
                C.name AS city_name, S.name AS state_name,
                W.name AS weight_class_name
                FROM user AS U
                LEFT JOIN city AS C on U.city = C.id LEFT JOIN state AS S on U.state = S.id
                INNER JOIN fighter AS F ON U.id = F.user_id
                LEFT JOIN record AS R ON F.id = R.fighter_id
                 LEFT JOIN weight_class AS W ON W.id = F.weight_class_id";

			/* WHERE U.state = :state */
			/* AND F.weight_class_id = :weight_class_id"; */

			if (!is_null($sqlQuery)) {
				$sql = $sql . $sqlQuery;
			}

			$statement = $pdo->prepare($sql);

			/* if(is_null($sqlQuery)){ */
			/*     $logger->info('sql query is null'); */
			/*     $statement->execute(); */
			/* }else{ */
			/*     /1* $statement->execute(array(':state' => $state , ':weight_class_id' => $weightClassId)); *1/ */
			/*     $logger->info('sql query is not null'); */
			/*     var_dump($queryParams); */
			/*     $statement->execute($queryParams); */
			/* } */
			$statement->execute($queryParams);

			$fighters['fighters'] = array();

			$resultSet = $statement->fetchAll();

			$temp = null;
			foreach ($resultSet as $result) {
				/* echo 'result'; */
				if (is_null($temp) || $result['id'] != $temp['id']) {
					/* echo 'temp is null or id are same'; */
					if (!is_null($temp)) {
						/* echo 'id are not same. saving previous entry'; */
						$fighters['fighters'][] = $temp;
					}
					$temp = array(
						'id' => $result['id'],
						'first_name' => $result['first_name'],
						'last_name' => $result['last_name'],
						'email' => $result['email'],
						'user_type' => $result['user_type'],
						'gender' => $result['gender'],
						'city' => $result['city'],
						'state' => $result['state'],
						'city_name' => $result['city_name'],
						'state_name' => $result['state_name'],
						'type_info' => array(
							'weight_class_name' => $result['weight_class_name'],
							'weight_class_id' => $result['weight_class_id'],
							'fight_team' => $result['fight_team'],
							'level' => $result['level'],
							'mma' => $result['mma'],
							'boxing' => $result['boxing'],
							'kickboxing' => $result['kickboxing'],
							'record' => array(),

						),
					);

					if (!is_null($result['fighter_id'])) {
						$temp['type_info']['record'][] = array(
							'id' => $result['rid'],
							'fight_type_id' => $result['fight_type_id'],
							'fighter_id' => $result['fighter_id'],
							'win' => $result['win'],
							'loss' => $result['loss'],
							'draw' => $result['draw'],
						);
					}
				} else {

					if (!is_null($result['fighter_id'])) {
						/* echo 'id are same appending to record'; */
						$temp['type_info']['record'][] = array(
							'id' => $result['rid'],
							'fight_type_id' => $result['fight_type_id'],
							'fighter_id' => $result['fighter_id'],
							'win' => $result['win'],
							'loss' => $result['loss'],
							'draw' => $result['draw'],
						);
					}
				}
				/* echo 'First Name: '.$result['first_name']; */
			}
			if (!is_null($temp)) {
				$fighters['fighters'][] = $temp;
			}
			echoResponse(200, $fighters);
			/* var_dump($fighters); */

			/* $rows = $stmt->fetchAll(PDO::FETCH_ASSOC); */
			/* foreach ($rows as $row) { */
			/*     echo $row['username']; */
			/* } */

		} else {
			//unauthorized
			$logger->info('Un authorized. User is not promoter');
			$response['message'] = "Un authorized user is not promoter";
			$response['status'] = 400;
			echoResponse(200, $response);
		}
	} else {
		//invalid params
		$logger->info('Invalid params');
		$response['message'] = "Invalid params";
		$response['status'] = 400;
		echoResponse(200, $response);
	}
}

?>
