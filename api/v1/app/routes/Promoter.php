<?php
require_once 'Models/UserModel.php';

$app->post('/promoter/:id','promoterForId');
$app->post('/promoter','getPromoterFor');
$app->post('/update_promoter','updatePromoter');

function getPromoterFor(){
	global $notOrm;
	global $app;
	$app->contentType('application/json');
    $body = $app->request->getBody();
    $requestParams = json_decode($body, true);
    $credentialPresent = verifyRequiredCredentials($requestParams);

    if ($credentialPresent) {
        $email = $requestParams['credential']['email'];
        $pass = $requestParams['credential']['pass'];
        $userObject = new UserModel($notOrm);
        $user = $userObject->authenticateUser($email , $pass);

        /* $user = $notOrm->user()-> */
        /*                 select(implode(',', Constant::$user_projection))-> */
        /*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); */
        if ($user && $user['user_type'] == USER_TYPE_PROMOTER) {
	        $response = getPromoter($notOrm, $user);
            unset($response['message']);
            unset($response['status']);
	        echoResponse(200, $response);
        }else{
        	echoResponse(200,array(
	            "status" => 400,
	            "message" => "Invalid credential"
	        ));	
        }
    }else{
    	echoResponse(200,array(
            "status" => 400,
            "message" => "Invalid Params"
        ));
    }
}

function promoterForId($id){
	global $notOrm;
	global $app;
	$app->contentType('application/json');
    $body = $app->request->getBody();
    $requestParams = json_decode($body, true);
    $credentialPresent = verifyRequiredCredentials($requestParams);

    if ($credentialPresent) {
        $email = $requestParams['credential']['email'];
        $pass = $requestParams['credential']['pass'];
        $userObject = new UserModel($notOrm);
        $user = $userObject->authenticateUser($email , $pass);

        /* $user = $notOrm->user()-> */
        /*                 select(implode(',', Constant::$user_projection))-> */
        /*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); */
        if ($user) {
        	$requestedPromoter = $notOrm->user()->
                        select(implode(',', Constant::$user_projection))->
                        where("id", $id)->fetch();
            if ($requestedPromoter && $requestedPromoter['user_type'] == USER_TYPE_PROMOTER){
		        $response = getPromoter($notOrm, $requestedPromoter);
                unset($response['message']);
                unset($response['status']);
		        echoResponse(200, $response);
            }else{
            	echoResponse(200,array(
		            "status" => 400,
		            "message" => "Requested promoter does not exists"
		        ));		
            }
        }else{
        	echoResponse(200,array(
	            "status" => 400,
	            "message" => "Invalid credential"
	        ));	
        }
    }else{
    	echoResponse(200,array(
            "status" => 400,
            "message" => "Invalid Params"
        ));
    }
}


function updatePromoter(){
    global $logger;
    global $notOrm;
    global $app;

    $logger->info("update Promoter");
    $app->contentType('application/json');
    $body = $app->request->getBody();
    $status = new Constant();
    $requestParams = json_decode($body, true);

    $paramsPresent = verifyUserParams($requestParams['user']);

    if(!$paramsPresent){
        $logger->info('Invalid Params');
        $response['status'] = 400;
        $response['message'] = "Invalid Params";
        echoResponse(200,$response);
        return;
    }

    $promoterParamsPresent = verifyPromoterParams($requestParams['user']['type_info']);

    $credentialPresent = verifyRequiredCredentials($requestParams);

    if ($credentialPresent && $promoterParamsPresent) {
        $logger->info('Credentials Present');
        $email = $requestParams['credential']['email'];
        $pass = $requestParams['credential']['pass'];
        $userObject = new UserModel($notOrm);
        $user = $userObject->authenticateUser($email , $pass);

        /* $user = $notOrm->user()-> */
        /*     select(implode(',', Constant::$user_projection))-> */
        /*     where("email = ? AND pass = ?", $email, $pass)->fetch(); */

        if($user && $user['id'] == $requestParams['user']['id']){
            $logger->info('params are valid');

            /* $userParam['user_id'] = $requestParams['user']['user_id']; */
            /* $userParam['email'] = $requestParams['user']['email']; */
            /* $userParam['user_type'] = $requestParams['user']['user_type']; */
            $userParam['first_name'] = $requestParams['user']['first_name'];
            $userParam['last_name'] = $requestParams['user']['last_name'];
            $userParam['pass'] = md5($requestParams['user']['pass']);
            $userParam['gender'] = $requestParams['user']['gender'];
            $userParam['city'] = $requestParams['user']['city'];
            $userParam['state'] = $requestParams['user']['state'];
            $userParam['push_notification_id'] = $requestParams['user']['push_notification_id'];
            $userParam['user_agent'] = $requestParams['user']['user_agent'];
            $userParam['update_time'] = date("Y-m-d H:i:s");


            $typeInfo = $requestParams['user']['type_info'];

            $promoterParams['promotion_name'] = $typeInfo['promotion_name'];


            $notOrm->transaction = "BEGIN";

            $updatedUser = $notOrm->user()->where('id', $user['id'])->fetch();
        
            $updatedUser->update($userParam);
            
            $updatedPromoter = $notOrm->promoter()->where('user_id',$user['id'])->fetch();
            $updatedPromoter->update($promoterParams);

            $updatedUser = $notOrm->user()->where('id', $user['id'])->fetch();
            $updatedPromoter = $notOrm->promoter()->where('user_id',$user['id'])->fetch();

            $notOrm->transaction = "COMMIT";



            $response['status'] = 200;
            $response['message']=  'Promoter updated sucessfully';
            $user = iterator_to_array($user);
            $updatedPromoter = iterator_to_array($updatedPromoter);
            $response['user'] = array(
                'type_info' => $updatedPromoter);
            $response['user'] = array_merge($response['user'],$user);

            echoResponse(200,$response);

        }else{
            //not permitted
            $logger->info('not permitted');
            $response['status'] = 400;
            $response['message']=  'Not permitted';
            echoResponse(200,$response);
        }
    }else{
        //invalid params
        $logger->info('invalid params');
        $response['status'] = 400;
        $response['message']=  'Invalid Params';
        echoResponse(200,$response);
    }
}

?>
