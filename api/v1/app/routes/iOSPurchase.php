<?php
require_once 'include/itunesReceiptValidator.php';

$app->post('/receipt', 'updateReceipt');
// define('PRODUCT_ID', 'com.medicalculations.MedicalCalculations.Tools');
define('FIGHTER_ID', 'com.tacktilesystems.promoter');
define('PROMOTER_ID', 'com.tacktilesystems.fighter');

function updateReceipt() {
	global $logger;
	global $app;
	global $notOrm;
	$logger->info("updateReceipt");
	$body = $app->request->getBody();
	$requestParams = json_decode($body, true);

	$credentialPresent = isset($requestParams['email']) && isset($requestParams['pass']);
	$receiptPresent = (isset($requestParams['receipt']) && (strlen($requestParams['receipt']) > 0));
	if ($credentialPresent && $receiptPresent) {

		$userObject = new UserModel($notOrm);
		$data = $userObject->authenticateUser($requestParams['email'], $requestParams['pass']);

		if ($data) {
			$receipt = $requestParams['receipt'];
			$logger->info($requestParams['email']);
			// $receiptValidator = new itunesreceiptvalidator(itunesreceiptvalidator::SANDBOX_URL, $receipt,'6974494f100044b891646362c2908088');
			$receiptValidator = new itunesreceiptvalidator(itunesreceiptvalidator::SANDBOX_URL, $receipt, 'f94e46fb3da04d81a3a7126e376b5b04');
			$purchaseData = $receiptValidator->validateReceipt();
			/* $purchaseData = json_decode($purchaseData, true); */
			$latestReceipt = NULL;
			$big = 0;
			foreach ($purchaseData['receipt']['in_app'] as $purchaseReceipt) {

				if ($purchaseReceipt['product_id'] == FIGHTER_ID ||
					$purchaseReceipt['product_id'] == PROMOTER_ID) {
					if ($purchaseReceipt['expires_date_ms'] > $big) {
						$big = $purchaseReceipt['expires_date_ms'];

						$latestReceipt = $purchaseReceipt;
					}
				}
			}
			if ($latestReceipt) {

				$previousReceipt = $notOrm->payment_ios_status()->where('user_id', $data['id'])->fetch();
				if ($previousReceipt) {
					$logger->info("updating previous receipt");
					$logger->info($receipt);
					/* echo "previous receipt found"; */
					//previous receipt exist update the previous receipt
					$previousReceipt['receipt'] = $receipt;
					$previousReceipt['expires_date'] = $latestReceipt['expires_date'];
					$previousReceipt['updated_at'] = date("Y-m-d H:i:s");
					$previousReceipt->update();
					// var_dump($data);
					// $updateParams['payment_status'] = 1;
					$data["purchase_status"] = 1;
					$affected = $data->update();
					// echo "affected rows";
					// var_dump($affected);
					// var_dump(iterator_to_array($data));
					$logger->info(json_encode(iterator_to_array($data)));

				} else {
					$logger->info("inserting new receipt");
					$logger->info($receipt);
					//no previous receipt insert new receipt
					/* echo "previous receipt not found"; */
					$iosPayment['user_id'] = $data['id'];
					$iosPayment['receipt'] = $receipt;
					$iosPayment['expires_date'] = $latestReceipt['expires_date'];
					$iosPayment['created_at'] = date("Y-m-d H:i:s");
					$iosPayment['updated_at'] = date("Y-m-d H:i:s");
					$result = $notOrm->payment_ios_status()->insert($iosPayment);

					$data["purchase_status"] = 1;
					$affected = $data->update();
					$logger->info(json_encode(iterator_to_array($data)));
					// echo "affected rows";
					// var_dump($affected);
					// var_dump(iterator_to_array($data));
				}

				$response['status'] = 200;
				$response['message'] = 'Successfully updated receipt';
				echoResponse(200, $response);
			} else {
				$response['status'] = 400;
				$response['message'] = "Recipt doesn't contain purchase";
				echoResponse(200, $response);

			}
		} else {
			//username password doesn't match or user doesn't exist
			//
			$logger->info("user is doesn't exist");
			//not permitted
			$response['status'] = 400;
			$response['message'] = 'Invalid user';
			echoResponse(200, $response);
		}
	} else {
		//required params not present
		//
		$logger->info("invalid user");
		//not permitted
		$response['status'] = 400;
		$response['message'] = 'Invalid Parameters';
		echoResponse(200, $response);
	}
}

?>
