<?php
require_once 'Models/UserModel.php';

$app->post('/user/:id','userForId');
$app->post('/user','getUserFor');
$app->post('/update_user','updateUser');

function getUserFor(){
	global $notOrm;
	global $app;
	$app->contentType('application/json');
    $body = $app->request->getBody();
    $requestParams = json_decode($body, true);
    $credentialPresent = verifyRequiredCredentials($requestParams);

    if ($credentialPresent) {
        $email = $requestParams['credential']['email'];
        $pass = $requestParams['credential']['pass'];
        $userObject = new UserModel($notOrm);
        $user = $userObject->authenticateUser($email , $pass);

        /* $user = $notOrm->user()-> */
        /*                 select(implode(',', Constant::$user_projection))-> */
        /*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); */
        if ($user) {
            $response = array('user' => $user );
	        echoResponse(200, $response);
        }else{
        	echoResponse(200,array(
	            "status" => 400,
	            "message" => "Invalid credential"
	        ));	
        }
    }else{
    	echoResponse(200,array(
            "status" => 400,
            "message" => "Invalid Params"
        ));
    }
}

function userForId($id){
	global $notOrm;
	global $app;
	$app->contentType('application/json');
    $body = $app->request->getBody();
    $requestParams = json_decode($body, true);
    $credentialPresent = verifyRequiredCredentials($requestParams);

    if ($credentialPresent) {
        $email = $requestParams['credential']['email'];
        $pass = $requestParams['credential']['pass'];
        $userObject = new UserModel($notOrm);
        $user = $userObject->authenticateUser($email , $pass);

        /* $user = $notOrm->user()-> */
        /*                 select(implode(',', Constant::$user_projection))-> */
        /*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); */
        if ($user) {
        	$requestedUser = $notOrm->user()->
                        select(implode(',', Constant::$user_projection))->
                        where("id", $id)->fetch();
            if ($requestedUser) {
		        $response = array('user' => $requestedUser );
		        echoResponse(200, $response);
            }else{
            	echoResponse(200,array(
		            "status" => 400,
		            "message" => "Requested user does not exists"
		        ));		
            }
        }else{
        	echoResponse(200,array(
	            "status" => 400,
	            "message" => "Invalid credential"
	        ));	
        }
    }else{
    	echoResponse(200,array(
            "status" => 400,
            "message" => "Invalid Params"
        ));
    }
}



function updateUser(){
    global $logger;
    global $notOrm;
    global $app;

    $logger->info("update User");
    $app->contentType('application/json');
    $body = $app->request->getBody();
    $status = new Constant();
    $requestParams = json_decode($body, true);

    $paramsPresent = verifyUserParams($requestParams['user']);

    if(!$paramsPresent){
        $logger->info('Invalid Params');
        $response['status'] = 400;
        $response['message'] = "Invalid Params";
        echoResponse(200,$response);
        return;
    }


    $credentialPresent = verifyRequiredCredentials($requestParams);

    if ($credentialPresent) {
        $logger->info('Credentials Present');
        $email = $requestParams['credential']['email'];
        $pass = $requestParams['credential']['pass'];
        $userObject = new UserModel($notOrm);
        $user = $userObject->authenticateUser($email , $pass);

        /* $user = $notOrm->user()-> */
        /*     select(implode(',', Constant::$user_projection))-> */
        /*     where("email = ? AND pass = ?", $email, $pass)->fetch(); */

        if($user && $user['id'] == $requestParams['user']['id'] 
          && $user['user_type'] == USER_TYPE_VIEWER){
            $logger->info('params are valid');

            /* $userParam['user_id'] = $requestParams['user']['user_id']; */
            /* $userParam['user_type'] = $requestParams['user']['user_type']; */
            /* $userParam['email'] = $requestParams['user']['email']; */
            $userParam['first_name'] = $requestParams['user']['first_name'];
            $userParam['last_name'] = $requestParams['user']['last_name'];
            $userParam['pass'] = md5($requestParams['user']['pass']);
            $userParam['gender'] = $requestParams['user']['gender'];
            $userParam['city'] = $requestParams['user']['city'];
            $userParam['state'] = $requestParams['user']['state'];
            $userParam['push_notification_id'] = $requestParams['user']['push_notification_id'];
            $userParam['user_agent'] = $requestParams['user']['user_agent'];
            $userParam['update_time'] = date("Y-m-d H:i:s");



            $updatedUser = $notOrm->user()->where('id', $user['id'])->fetch();
        
            $updatedUser->update($userParam);
            
            $updatedUser = $notOrm->user()->where('id', $user['id'])->fetch();

            $response['status'] = 200;
            $response['message']=  'User updated sucessfully';
            $updatedUser = iterator_to_array($updatedUser);
            $response['user'] = array();
            $response['user'] = array_merge($response['user'],$updatedUser);

            echoResponse(200,$response);

        }else{
            //not permitted
            $logger->info('not permitted');
            $response['status'] = 400;
            $response['message']=  'Not permitted';
            echoResponse(200,$response);
        }
    }else{
        //invalid params
        $logger->info('invalid params');
        $response['status'] = 400;
        $response['message']=  'Invalid params.';
        echoResponse(200,$response);
    }
}


?>
