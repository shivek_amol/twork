<?php

require_once 'include/Constant.php';
require_once 'include/Utils.php';
require_once 'Models/UserModel.php';

/**
 * @apiDefine Credential
 * @apiParam {Object} credential Credential
 * @apiParam {String} credential.email Email id
 * @apiParam {String} credential.pass Password
 */

/**
 * @apiDefine UserSucces
 * @apiSuccess {Object[]} event_participants User details
 * @apiSuccess {Number} event_participants.user_id User id
 * @apiSuccess {String} event_participants.first_name First Name of the user
 * @apiSuccess {String} event_participants.last_name Last name
 * @apiSuccess {String} event_participants.email Email id
 * @apiSuccess {Number} event_participants.user_type
 * @apiSuccess {String} event_participants.gender
 * @apiSuccess {String} event_participants.city
 * @apiSuccess {String} event_participants.state
 *
 */

/**
 * @api {post} /add_event_participant Add Event participant
 * @apiName AddEventParticipant
 * @apiGroup Event
 *
 * @apiUse Credential
 *
 * @apiParam {Object} event_participant Event Participant
 * @apiParam {Number} event_participant.event_id Id of the event in which participating
 * @apiParam {Number} event_participant.user_agent 1 for Android 0 for iOS
 *
 *
 * @apiSuccess {String} message Message
 * @apiSuccess {Number} status  200
 */
$app->post("/add_event_participant", "addEventParticipant");

/**
 * @api {post} /get_event_participant Get Event participant by filters
 * @apiName GetEventParticipant
 * @apiGroup Event
 *
 * @apiUse Credential
 *
 * @apiParam {Object} filter Filter for filtering event participants
 * @apiParam {Number} filter.event_id Event id for which Event participants are needed
 * @apiParam {Number} filter.user_id User id for which Event participants are needed
 *
 * @apiSuccess {Number} status  200
 * @apiUse UserSucces
 */
$app->post("/get_event_participant", "getEventParticipant");

/**
 * @api {post} /remove_event_participant Remove Particpant from Event
 * @apiName RemoveEventParticipant
 * @apiGroup Event
 *
 * @apiUse Credential
 *
 * @apiParam {Object} event_participant Filter for filtering event Participants
 * @apiParam {Number} event_participant.event_id Id of the event from which to remove
 * @apiParam {Number} event_participant.user_agent 1 for Android 0 for iOS
 *
 * @apiSuccess {String} message Message
 * @apiSuccess {Number} status  200
 */
$app->post("/remove_event_participant", "removeEventParticipant");

$app->post("/has_viewer_participated", "hasViewerParticipated");

function addEventParticipant() {
	global $app;
	global $notOrm;

	$app->contentType('application/json');
	$body = $app->request->getBody();
	$requestParams = json_decode($body, true);

	$requiredParams = array("event_id");
	$credentialPresent = verifyRequiredCredentials($requestParams);
	$eventParamPresent = verifyRequiredParams(array('event_participant'), $requestParams);
	$participantParamPresent = FALSE;

	if ($eventParamPresent) {
		$participantParamPresent = verifyRequiredParams($requiredParams, $requestParams['event_participant']);
	} else {
		$response = "Invalid params";
		echoResponse(400, composeErrorResponse(400, $response));
		return;
	}

	if ($eventParamPresent && $credentialPresent) {
		$email = $requestParams['credential']['email'];
		$pass = $requestParams['credential']['pass'];
		$userObject = new UserModel($notOrm);
		$user = $userObject->authenticateUser($email, $pass);

		/* $user = $notOrm->user()-> */
		/*                 select(implode(',', Constant::$user_projection))-> */
		/*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); */

		if ($user && $user['user_type'] == USER_TYPE_VIEWER) {
			$participant = extractVariables($requiredParams, $requestParams['event_participant']);
			$participant['user_id'] = $user['id'];
			$alreadyParticipated = $notOrm->event_participant()->where($participant)->fetch();

			$result;
			if ($alreadyParticipated) {
				$result = iterator_to_array($alreadyParticipated);
			} else {
				$participant['updated_time'] = date("Y-m-d H:i:s");
				$result = insertEventParticipant($notOrm, $participant);
				$result = iterator_to_array($result);
			}
			$response['message'] = "Event Participant added";
			$response['status'] = 200;
			$response['event_participant'] = $result;
			echoResponse(200, $response);
		} else {
			$response = "Invalid Credentials";
			echoResponse(400, composeErrorResponse(400, $response));
		}
	} else {
		$message = "Invalid Request";
		if (!$credentialPresent) {
			$message = "Credentials not present";
		} else if (!$eventParamPresent) {
			$message = getErrorMsg($requiredParams, $requestParams['event_participant']);
		}
		echoResponse(400, composeErrorResponse(400, $response));
	}
}

function getEventParticipant() {
	global $app;
	global $notOrm;

	$app->contentType('application/json');
	$body = $app->request->getBody();
	$requestParams = json_decode($body, true);

//    $requiredParams = array("event_id", "user_agent");
	$credentialPresent = verifyRequiredCredentials($requestParams);
	$filterPresent = verifyRequiredParams(array('filter'), $requestParams);
	$eventParamPresent = FALSE;
	if ($filterPresent) {
		$eventParamPresent = verifyRequiredParams(array('event_id'), $requestParams['filter']) ||
		verifyRequiredParams(array('user_id'), $requestParams['filter']);
	} else {
		$message = "Filter missing";
		echoResponse(400, composeErrorResponse(400, $message));
		return;
	}

	if ($credentialPresent && $eventParamPresent) {
		$email = $requestParams['credential']['email'];
		$pass = $requestParams['credential']['pass'];
		$userObject = new UserModel($notOrm);
		$user = $userObject->authenticateUser($email, $pass);

		/* $user = $notOrm->user()-> */
		/*                 select(implode(',', Constant::$user_projection))-> */
		/*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); */
		if ($user) {
			$filter = array_filter($requestParams['filter'], 'filterZeroes');
			$eventParticipants = $notOrm->event_participant()->order('user.first_name')->where($filter);
//            $response = array('event_participants'=> array_map('iterator_to_array', iterator_to_array($eventParticipants)));
			$response['event_participants'] = array();
			foreach ($notOrm->event_participant()->order('user.first_name')->where($filter) as $ep) {
//                echo $ep['id']." ";
				//                echo $ep->user['last_name'];
				array_push($response['event_participants'], array(
					"id" => $ep->user['id'],
					"first_name" => $ep->user['first_name'],
					"last_name" => $ep->user['last_name'],
					"email" => $ep->user['email'],
					"pass" => $ep->user['pass'],
					"user_type" => $ep->user['user_type'],
					"gender" => $ep->user['gender'],
					"city" => $ep->user['city'],
					"state" => $ep->user['state'],
					"push_notification_id" => $ep->user['push_notification_id'],
					"user_agent" => $ep->user['user_agent'],
					"update_time" => $ep->user['update_time'],
					"del_flag" => $ep->user['del_flag'],
				));
			}

			echoResponse(200, $response);
		} else {
			$response = "Invalid Credentials";
			echoResponse(400, composeErrorResponse(400, $response));
		}
	}
}

function removeEventParticipant() {
	global $app;
	global $notOrm;

	$app->contentType('application/json');
	$body = $app->request->getBody();
	$requestParams = json_decode($body, true);

	$requiredParams = array("event_id");
	$credentialPresent = verifyRequiredCredentials($requestParams);
	$eventParamPresent = verifyRequiredParams(array('event_participant'), $requestParams);
	$participantParamPresent = FALSE;

	if ($eventParamPresent) {
		$participantParamPresent = verifyRequiredParams($requiredParams, $requestParams['event_participant']);
	} else {
		$response = "Invalid params";
		echoResponse(400, composeErrorResponse(400, $response));
		return;
	}

	if ($eventParamPresent && $credentialPresent) {
		$email = $requestParams['credential']['email'];
		$pass = $requestParams['credential']['pass'];
		$userObject = new UserModel($notOrm);
		$user = $userObject->authenticateUser($email, $pass);

		/* $user = $notOrm->user()-> */
		/*                 select(implode(',', Constant::$user_projection))-> */
		/*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); */

		if ($user && $user['user_type'] == USER_TYPE_VIEWER) {
			$participant = extractVariables($requiredParams, $requestParams['event_participant']);
			$participant['user_id'] = $user['id'];
			unset($participant['user_agent']);
			$prevParticipant = $notOrm->event_participant()->where($participant)->fetch();
			$result;

			if ($prevParticipant) {
				$result = deleteEventParticipant($notOrm, $participant);
				// $result = $prevParticipant;
				// $response['event_participant'] = $result;
				$response['message'] = "Event Participant Removed";
				$response['status'] = 200;
			} else {
				// $result = insertEventFighter($notOrm, $participant);
				$response['message'] = "You have not participated in this event";
				$response['status'] = 400;
			}

			echoResponse(200, $response);
		} else {
			$response = "Error removing participant";
			echoResponse(400, composeErrorResponse(400, $response));
		}
	} else {
		$message = "Invalid Request";
		if (!$credentialPresent) {
			$message = "Invalid Credentials";
		} else if (!$eventParamPresent) {
			$message = getErrorMsg($requiredParams, $requestParams['event_participant']);
		}
		echoResponse(400, composeErrorResponse(400, $response));
	}
}

function hasViewerParticipated() {

	global $app;
	global $notOrm;

	$app->contentType('application/json');
	$body = $app->request->getBody();
	$requestParams = json_decode($body, true);

//    $requiredParams = array("event_id", "user_agent");
	$credentialPresent = verifyRequiredCredentials($requestParams);
	$filterPresent = verifyRequiredParams(array('filter'), $requestParams);
	$eventIdPresent = FALSE;
	$userIdPresent = FALSE;
	if ($filterPresent) {
		$eventParamPresent = verifyRequiredParams(array('event_id'), $requestParams['filter']);
		$userIdPresent = isset($requestParams['filter']['user_id']) && $requestParams['filter']['user_id'] > 0;
	} else {
		$response = "Invalid params";
		echoResponse(200, composeErrorResponse(400, $response));
		return;
	}

	if ($credentialPresent) {
		$email = $requestParams['credential']['email'];
		$pass = $requestParams['credential']['pass'];
		$userObject = new UserModel($notOrm);
		$user = $userObject->authenticateUser($email, $pass);

		/* $user = $notOrm->user()-> */
		/*                 select(implode(',', Constant::$user_projection))-> */
		/*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); */

		if ($user) {
			$requestedUser;
			if ($userIdPresent) {
				$requestedUser = $notOrm->user()->where('id', $requestParams['filter']['user_id'])->fetch();
			} else {
				$requestedUser = $user;
			}

			$eventParticipant = $notOrm->event_participant()->where('user_id = ? AND event_id = ?', $requestedUser['id'], $requestParams['filter']['event_id'])->fetch();
			if ($eventParticipant) {
				$response['has_participated'] = true;
				echoResponse(200, $response);
			} else {
				$response['has_participated'] = false;
				echoResponse(200, $response);
			}
		} else {
			$response = "Invalid credential";
			echoResponse(200, composeErrorResponse(400, $response));
			return;
		}
	} else {
		$response = "Invalid credential";
		echoResponse(200, composeErrorResponse(400, $response));
		return;
	}

}

function insertEventParticipant($db, $participantParams) {
	return $db->event_participant()->insert($participantParams);
}

function deleteEventParticipant($db, $participant) {
	return $db->event_participant()->where('user_id', $participant['user_id'])->fetch()->delete();
}
