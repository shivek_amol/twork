<?php
/**
 * @api {post} /update_push_notification_id Update Registration id of user
 * @apiDescription Update the Registration ID of the device for user, fighter and promoter
 * @apiName UpdatePushNotificationId
 * @apiGroup User
 *
 * @apiUse Credential
 *
 * @apiParam {String} push_notification_id The push notification id
 *
 * @apiSuccess {String} message Message
 * @apiSuccess {Number} status  200
 *
 * @apiError {String} message Reason for failure
 * @apiError {String} status
 */
$app->post('/update_push_notification_id','updatePushNotificationId');

function updatePushNotificationId(){
    global $app;
    global $notOrm;

    $app->contentType('application/json');
    $body = $app->request->getBody();
    $requestParams = json_decode($body, true);
    $credentialPresent = verifyRequiredCredentials($requestParams);

    if(isset($requestParams['push_notification_id']) && strlen($requestParams['push_notification_id']) > 0){
        $email = $requestParams['credential']['email'];
        $pass = md5($requestParams['credential']['pass']);

        $user = $notOrm->user()->
                        select(implode(',', Constant::$user_projection))->
                        where("email = ? AND pass = ?", $email, $pass)->fetch();
        if($user){
            $update['push_notification_id'] = $requestParams['push_notification_id'];
            $user->update($update);
            $response['status'] = 200;
            $response['message'] = 'Push Notification id updated sucessfully';
            echoResponse(200, $response);
        }else{
            $response['status'] = 400;
            $response['message'] = 'Invalid credentials';
            echoResponse(200, $response);
        }

    }else{
        $response['status'] = 400;
        $response['message'] = 'Invalid params';
        echoResponse(200, $response);
    }
}

?>
