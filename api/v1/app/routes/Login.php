<?php

// require_once 'DbLoginHandler.php';
require_once 'Models/UserModel.php';

global $app;
global $pdo;
global $notOrm;
global $logger;

/**
 * @api {post} /login Request User login
 * @apiName Login
 * @apiGroup User
 *
 * @apiParam {String} email Email ID.
 * @apiParam {String} pass Password.
 *
 * @apiSuccess {Object} user User details
 * @apiSuccess {Number} user.user_id User id
 * @apiSuccess {String} user.first_name First Name of the user
 * @apiSuccess {String} user.last_name Last name
 * @apiSuccess {String} user.email Email id
 * @apiSuccess {Number} user.user_type
 * @apiSuccess {String} user.gender
 * @apiSuccess {String} user.city
 * @apiSuccess {String} user.state
 *
 *
 * @apiSuccess {Object} type_info Only if the user loggin is Fighter
 * @apiSuccess {Number} type_info.fighter_id Fighter id
 * @apiSuccess {Number} type_info.user_id
 * @apiSuccess {Number} type_info.weight_class_id Weight class of the fighter
 * @apiSuccess {Number} type_info.mma 1 if fighter fights mma 0 if not
 * @apiSuccess {Number} type_info.boxing if fighter fights boxing 0 if not
 * @apiSuccess {Number} type_info.kickboxing if fighter fights kickboxing 0 if not
 *
 * @apiSuccess {Object[]} type_info.record Record for fight
 * @apiSuccess {String} type_info.record.id
 * @apiSuccess {String} type_info.record.fighter_id Id of fighter
 * @apiSuccess {String} type_info.record.fight_type_id Type of fight for which record exists
 * @apiSuccess {String} type_info.record.draw Number of draws
 * @apiSuccess {String} type_info.record.win Number of wins
 * @apiSuccess {String} type_info.record.loss Number of lossess
 *
 *
 * @apiSuccess {Number} type_info.promoter_id
 * @apiSuccess {Number} type_info.user_id
 * @apiSuccess {String} type_info.promotion_name Name of the promotion
 *
 * @apiSuccess {String} message Message
 * @apiSuccess {String} status  Status
 */
$app->post('/login', function () use ($app) {
	global $logger;
	$logger->info("login fucntion");
//    $app->contentType('application/json');
	$body = $app->request->getBody();
//    var_dump($body);
	$login = json_decode($body, true);

	global $notOrm;
	global $pdo;
	$user_projection = array("id", "first_name", "last_name", "email", "user_type", "gender", "city", "state");
/* $result = $notorm->user()->select(implode(',',constant::$user_projection)) *//*     ->where("email = ? and pass = ?", $login['email'], md5($login['pass'])); *//* $data = $result->fetch(); */
	$userObject = new UserModel($notOrm);
	$data = $userObject->authenticateUser($login['email'], $login['pass']);

	if ($data) {

		if (isset($login['push_notification_id']) && strlen($login['push_notification_id'])) {
			$data['push_notification_id'] = $login['push_notification_id'];
			$data->update();
		}

		// if($data['purchase_status'] == NOT_PURCHASED && $data['user_type'] != USER_TYPE_VIEWER){
		//     $response['status'] = 403;
		//     $response['message'] = "You have not subscribed for this service";
		//     echoResponse(200, $response);
		//     return;
		// }

		$logger->info('user exits. valid credential');
		// var_dump($data['id']);
		$data = iterator_to_array($data);
		$response = NULL;

		switch ($data['user_type']) {
			case USER_TYPE_VIEWER:
				$logger->info("user is view type");
				$response = array('user' => $data);
				$response['message'] = 'Login Successfull';
				$response['status'] = 200;
				break;
			case USER_TYPE_FIGHTER:
				$logger->info("user is fighter");
				$response = getFighter($pdo, $data);
				break;
			case USER_TYPE_PROMOTER:
				$logger->info("user is promoter");
				$response = getPromoter($notOrm, $data);
				break;
			default:
				break;
		}
		if ($data['purchase_status'] == NOT_PURCHASED && $data['user_type'] != USER_TYPE_VIEWER) {
			$response['status'] = 403;
		}
		echoResponse(200, $response);
	} else {
		$logger->info("user does not exists");
		echoResponse(200, array(
			"status" => 400,
			"message" => "User does not exist",
		));
	}
});

$app->post('/logout', function () use ($app) {

	global $logger;
	$logger->info("login fucntion");
	$body = $app->request->getBody();
	$requestParams = json_decode($body, true);
	global $notOrm;

	$credentialPresent = isset($requestParams['email']) && isset($requestParams['pass']);
	if ($credentialPresent) {
		$userObject = new UserModel($notOrm);
		$data = $userObject->authenticateUser($requestParams['email'], $requestParams['pass']);

		if ($data) {

			// if($data['purchase_status'] == NOT_PURCHASED && $data['user_type'] != USER_TYPE_VIEWER){
			//     $response['status'] = 403;
			//     $response['message'] = "You have not subscribed for this service";

			//     echoResponse(200, $response);
			//     return;
			// }

			$data['push_notification_id'] = '';
			$data->update();
			$response['status'] = 200;
			$response['message'] = 'Logged out successfully';
			echoResponse(200, $response);

		} else {
			echoResponse(200, array(
				'status' => 400,
				'message' => 'Invalid Credentials'));
		}
	} else {
		echoResponse(200, array(
			'status' => 400,
			'message' => 'Invalid Params'));
	}
});

$app->post('/forgot_password', function () use ($app) {
	global $logger;
	$logger->info("login fucntion");
	$body = $app->request->getBody();
	$requestParams = json_decode($body, true);
	global $notOrm;
	//	echo $requestParams;
	if (isset($requestParams['email'])) {
		$user = $notOrm->user->where('email', $requestParams['email'])->fetch();

		$randomPassword = "abcd";
		$tokentext = "cageread@";
		$token_num = rand(9, 9999);;
		$token = $tokentext.$token_num;
		//echo "token = ". $token ;
		
		$user['forgot_pass'] = md5($token);
		$user->update();

		$to = $requestParams['email'];
		$subject = "Reset password link from cagereadycombatives.com";
		$txt = "A request to reset the password for your cagereadycombatives.com account has been 
		made.You may now clicking this link or copying and pasting it to your
		browser:" ."\n"."http://www.cagereadycombatives.com/forgot_pass.php?email=".$requestParams['email']."&fgtpass=".md5($token)."\n
		This link can only be used once to lead you to a page where you can set your password. \n 
		--  cagereadycombatives.com team ." ;
		$headers = "From: no-reply@cagereadycombatives.com" . "\r\n" ;

		mail($to,$subject,$txt,$headers);



		$response['status'] = 200;
		$response['message'] = 'Password sent to mail';

		echoResponse(200, $response);
	} else {
		$response['status'] = 400;
		$response['message'] = 'Invalid params..!';
		echoResponse(200, $response);

	}

});

function getPromoter($db, $user) {
	global $logger;
	$logger->info("getPromoter");
	$response = array();

	$promoter = $db->promoter()->where('user_id', $user['id'])->fetch();

	$promoter = iterator_to_array($promoter);
	$response['user'] = $user;
	$response['user']['type_info'] = $promoter;
	$response['message'] = 'Login Successfull';
	$response['status'] = 200;
	return $response;
}

function getFighter($db, $user) {
	global $logger;
	$logger->info("getFighter");

	$sql = "SELECT U.id,U.first_name,U.last_name,U.email,U.pass,U.user_type,U.gender,U.city,U.state,
                U.push_notification_id,U.user_agent,U.update_time,U.del_flag,
                F.id AS fid,F.user_id,F.weight_class_id,F.fight_team,F.update_time,F.mma,F.boxing,F.kickboxing,F.level,
                R.id AS rid,R.fighter_id,R.fight_type_id,R.draw,R.win,R.loss,
                C.name AS city_name, S.name AS state_name
                FROM user AS U
                LEFT JOIN city AS C on U.city = C.id LEFT JOIN state AS S on U.state = S.id
                INNER JOIN fighter AS F ON U.id = F.user_id
                LEFT JOIN record AS R ON R.fighter_id = F.id
                WHERE U.id = :user_id";

	$stmt = $db->prepare($sql);
	$stmt->execute(array(':user_id' => $user['id']));

	$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$temp = null;
	$fighter = null;

	foreach ($resultSet as $result) {
		/* echo 'result'; */
		if (is_null($temp) || $result['id'] != $temp['id']) {
			/* echo 'temp is null or id are same'; */
			if (!is_null($temp)) {
				/* echo 'id are not same. saving previous entry'; */
				// $fighters['fighters'][] = $temp;
				$fighter = $temp;
			}
			$temp = array(
				'id' => $result['id'],
				'first_name' => $result['first_name'],
				'last_name' => $result['last_name'],
				'email' => $result['email'],
				'user_type' => $result['user_type'],
				'gender' => $result['gender'],
				'city' => $result['city'],
				'state' => $result['state'],
				'type_info' => array(
					'id' => $result['fid'],
					'weight_class_id' => $result['weight_class_id'],
					'fight_team' => $result['fight_team'],
					'level' => $result['level'],
					'mma' => $result['mma'],
					'boxing' => $result['boxing'],
					'kickboxing' => $result['kickboxing'],
					'record' => array(),

				),
			);

			if (!is_null($result['fighter_id'])) {
				$temp['type_info']['record'][] = array(
					'id' => $result['rid'],
					'fight_type_id' => $result['fight_type_id'],
					'fighter_id' => $result['fighter_id'],
					'win' => $result['win'],
					'loss' => $result['loss'],
					'draw' => $result['draw'],
				);
			}
		} else {

			if (!is_null($result['fighter_id'])) {
				/* echo 'id are same appending to record'; */
				$temp['type_info']['record'][] = array(
					'id' => $result['rid'],
					'fight_type_id' => $result['fight_type_id'],
					'fighter_id' => $result['fighter_id'],
					'win' => $result['win'],
					'loss' => $result['loss'],
					'draw' => $result['draw'],
				);
			}
		}
		/* echo 'First Name: '.$result['first_name']; */
	}
	if (!is_null($temp)) {
		$fighter = $temp;
	}

	// $response = array();
	// $fighter = $db->fighter()->where('user_id', $user['id'])->fetch();
	// $record = $db->record()->where('fighter_id', $fighter['id']);

	// $recordArray = array_map('iterator_to_array', iterator_to_array($record));

	// $response['user'] = $user;
	// $response['user']['type_info'] = array(
	//     'id' => $fighter['id'], 'user_id' => $fighter['user_id'],
	//     'fight_team' => $fighter['fight_team'],
	//     'weight_class_id' => $fighter['weight_class_id'],
	//     'mma' => $fighter['mma'], 'boxing' => $fighter['boxing'],
	//     'kickboxing' => $fighter['kickboxing'],
	//     'record' => array_values($recordArray),'level'=>$fighter['level']);
	$response['user'] = $fighter;
	$response['message'] = 'Login Successfull';
	$response['status'] = 200;

	return $response;
}

?>
