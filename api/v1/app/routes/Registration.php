<?php

global $app;
global $logger;

$app->post('/registration', function() use ($app) {
    global $logger;
    global $notOrm;

    $logger->info("registration");
    $app->contentType('application/json');
    $body = $app->request->getBody();
    $status = new Constant();
    $requestParams = json_decode($body, true);

    $paramsPresent = verifyParams($requestParams);

    if(!$paramsPresent){
        $logger->info('Invalid Params');
        $response['status'] = 400;
        $response['message'] = "Invalid Params";
        echoResponse(200,$response);
        return;
    }

    $emailAvailable = isEmailAvailable($notOrm, $requestParams['user']['email']);

    if(!$emailAvailable){
        $logger->info('Email already taken');
        $response['status'] = 400;
        $response['message'] = "Please user another email address";
        echoResponse(200,$response);
        return;
    }

    $logger->info('params are valid');

    /* $userParam['user_id'] = $requestParams['user']['user_id']; */
    $userParam['first_name'] = $requestParams['user']['first_name'];
    $userParam['last_name'] = $requestParams['user']['last_name'];
    $userParam['email'] = $requestParams['user']['email'];
    $userParam['pass'] = md5($requestParams['user']['pass']);
    $userParam['user_type'] = $requestParams['user']['user_type'];
    $userParam['gender'] = $requestParams['user']['gender'];
    $userParam['city'] = $requestParams['user']['city'];
    $userParam['state'] = $requestParams['user']['state'];
    $userParam['push_notification_id'] = $requestParams['user']['push_notification_id'];
    $userParam['user_agent'] = $requestParams['user']['user_agent'];
    $userParam['update_time'] = date("Y-m-d H:i:s");
    
    

    /* if($user){ */
    if($userParam['user_type'] == USER_TYPE_FIGHTER){
        $logger->info('user is fighter');
        $typeInfo = $requestParams['user']['type_info'];

        $fighterParams['weight_class_id'] = $typeInfo['weight_class_id'];
        $fighterParams['fight_team'] = $typeInfo['fight_team'];
        $fighterParams['update_time'] = date("Y-m-d H:i:s");
        $fighterParams['mma'] = $typeInfo['mma'];
        $fighterParams['boxing'] = $typeInfo['boxing'];
        $fighterParams['kickboxing'] = $typeInfo['kickboxing'];
        $fighterParams['level'] = $typeInfo['level'];
        

        $notOrm->transaction = "BEGIN";
        $user = $notOrm->user()->insert($userParam);
        /* $user = $notOrm->user()->insert_update(array('id'=>$useParam['user_id']), $userParam, $userParam); */
        $fighterParams['user_id'] = $user['id'];
        $fighter = $notOrm->fighter()->insert($fighterParams);
        /* $fighter = $notOrm->fighter()->insert_update(array('id' => $fighterParams['user_id']), */ 
            /* $fighterParams, $fighterParams); */

        $recordsParams = array();
        $records = array();
        /* var_dump(iterator_to_array($fighter)); */
        foreach($typeInfo['record'] as $record){
            /* $recordsParams[] = array( */
            /*     'fighter_id' => $fighter['id'], */
            /*     'win' => $record['win'], */
            /*     'loss' => $record['loss'], */
            /*     'draw' => $record['draw'], */
            /*     'fight_type_id' => $record['fight_type_id']); */
            $record['fighter_id'] = $fighter['id'];
            $insertedRecord = $notOrm->record()->insert($record);
            $insertedRecord = iterator_to_array($insertedRecord);
            $records[] = $insertedRecord;
        }
        
        /* $records = call_user_func_array('$notOrm->record()->insert',$recordsParams); */
        $user = $notOrm->user()->where('id',$user['id'])->fetch();
        $fighter = $notOrm->fighter->where('id',$fighter['id'])->fetch();
        $notOrm->transaction = "COMMIT";

        $fighterArr = iterator_to_array($fighter);

        $fighterArr['record'] = $records;

        $response['status'] = 200;
        $response['message']=  'Fighter inserted sucessfully';
        $user = iterator_to_array($user);
        /* $response['user'] = array($user, */
        $response['user'] = array(
            'type_info' => $fighterArr);
        $response['user'] = array_merge($response['user'],$user);

        echoResponse(200,$response);

    }

    if($userParam['user_type'] == USER_TYPE_PROMOTER){
        $typeInfo = $requestParams['user']['type_info'];
        $promoterParams['promotion_name'] = $typeInfo['promotion_name'];
        $promoterParams['updated_time'] = date("Y-m-d H:i:s");

        $notOrm->transaction = "BEGIN";
        $user = $notOrm->user()->insert($userParam);
        /* $user = $notOrm->user()->insert_update(array('id' => $userParam['user_id']), $userParam, $userParam); */
        $promoterParams['user_id'] = $user['id'];
        $promoter = $notOrm->promoter()->insert($promoterParams);
        /* $promoter = $notOrm->promoter()->insert_update(array('id',$user['id']), */
            /* $promoterParams, $promoterParams); */
        $user = $notOrm->user()->where('id', $user['id'])->fetch();
        $promoter = $notOrm->promoter()->where('id',$promoter['id'])->fetch();
        $notOrm->transaction = "COMMIT";

        $response['status'] = 200;
        $user = iterator_to_array($user);
        $promoter = iterator_to_array($promoter);
        $response['message']=  'Promoter inserted sucessfully';
        $response['user'] = array('type_info' => $promoter);
        $response['user'] = array_merge($response['user'],$user);

        echoResponse(200,$response);

    }

    if($userParam['user_type'] == USER_TYPE_VIEWER){
        $user = $notOrm->user()->insert($userParam);
        /* $user = $notOrm->user()->insert_update(array('id' => $requestParams['user']['user_id']), */
        /*     $userParam, $userParam); */
        $user = $notOrm->user()->where('id',$user['id'])->fetch();
        $response['status'] = 200;
        $user = iterator_to_array($user);
        $response['message']=  'User inserted sucessfully';
        /* $response['user'] = array(); */
        /* $response['user'] = array_merge($response['user'],$user); */
        $response['user'] = $user;

        echoResponse(200,$response);

    }
    /* } */

    
    /* echoResponse(200, $response); */
});


function isEmailAvailable($db, $email){
    $user = $db->user()->where('email',$email)->fetch();
    return !$user;
}
?>
