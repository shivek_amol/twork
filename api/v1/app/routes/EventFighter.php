<?php

require_once 'include/Constant.php';
require_once 'include/Utils.php';
require_once 'Models/UserModel.php';

/**
 * @apiDefine FighterSuccess
 * @apiSuccess {Object[]} event_participants
 * @apiSuccess {Number} event_participants.user_id User id
 * @apiSuccess {String} event_participants.first_name First Name of the user
 * @apiSuccess {String} event_participants.last_name Last name
 * @apiSuccess {String} event_participants.email Email id
 * @apiSuccess {Number} event_participants.user_type
 * @apiSuccess {String} event_participants.gender
 * @apiSuccess {String} event_participants.city
 * @apiSuccess {String} event_participants.state
 * @apiSuccess {Object} event_participants.type_info Only if the user loggin is Fighter
 * @apiSuccess {Number} event_participants.type_info.fighter_id Fighter id
 * @apiSuccess {Number} event_participants.type_info.user_id
 * @apiSuccess {Number} event_participants.type_info.weight_class_id Weight class of the fighter
 * @apiSuccess {Number} event_participants.type_info.mma 1 if fighter fights mma 0 if not
 * @apiSuccess {Number} event_participants.type_info.boxing if fighter fights boxing 0 if not
 * @apiSuccess {Number} event_participants.type_info.kickboxing if fighter fights kickboxing 0 if not
 *
 *
 */

/**
 * @api {post} /add_event_fighter Add Event fighter
 * @apiName AddEventFighter
 * @apiGroup Event
 *
 * @apiUse Credential
 *
 * @apiParam {Object} event_fighter Event Fighter
 * @apiParam {Number} event_fighter.event_id Id of the event in which participating
 * @apiParam {Number} event_fighter.user_agent 1 for Android 0 for iOS
 *
 *
 * @apiSuccess {String} message Message
 * @apiSuccess {Number} status  200
 */
$app->post("/add_event_fighter", "addEventFighters");

/**
 * @api {post} /get_event_fighter Get Event Fighter by filters
 * @apiName GetEventFighter
 * @apiGroup Event
 *
 * @apiUse Credential
 *
 * @apiParam {Object} filter Filter for filtering event Fighters
 * @apiParam {Number} filter.event_id Event id for which Event fighters are needed
 * @apiParam {Number} filter.fighter_id User id for which Event fighters are needed
 *
 * @apiUse FighterSuccess
 */
$app->post("/get_event_fighter", "getEventFighters");

/**
 * @api {post} /remove_event_fighter Remove Fighter from Event
 * @apiName RemoveEventFighter
 * @apiGroup Event
 *
 * @apiUse Credential
 *
 * @apiParam {Object} event_fighter Filter for filtering event Fighters
 * @apiParam {Number} event_fighter.event_id Id of the event from which to remove
 * @apiParam {Number} event_fighter.user_agent 1 for Android 0 for iOS
 *
 *
 */
$app->post("/remove_event_fighter", "removeEventFighter");

/**
 * @api {post} /has_fighter_participated Check if Fighter participated in Event
 * @apiName HasFighterParticipated
 * @apiGroup Event
 *
 * @apiUse Credential
 *
 * @apiParam {Object} filter
 * @apiParam {Number} filter.event_id
 *
 *
 * @apiSuccess {Boolean} has_participated
 *
 *
 */
$app->post("/has_fighter_participated", "hasFighterParticipated");

function addEventFighters() {
	global $app;
	global $notOrm;

	$app->contentType('application/json');
	$body = $app->request->getBody();
	$requestParams = json_decode($body, true);

	$requiredParams = array("event_id");
	$credentialPresent = verifyRequiredCredentials($requestParams);
	$eventParamPresent = verifyRequiredParams(array('event_fighter'), $requestParams);
	$fighterParamPresent = FALSE;

	if ($eventParamPresent) {
		$fighterParamPresent = verifyRequiredParams($requiredParams, $requestParams['event_fighter']);
	} else {
		$response = "Invalid params";
		echoResponse(200, composeErrorResponse(400, $response));
		return;
	}

	if ($eventParamPresent && $credentialPresent) {
		$email = $requestParams['credential']['email'];
		$pass = $requestParams['credential']['pass'];

		/* $user = $notOrm->user()-> */
		/*                 select(implode(',', Constant::$user_projection))-> */
		/*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); */
		$userObject = new UserModel($notOrm);
		$user = $userObject->authenticateUser($email, $pass);

		if ($user && $user['user_type'] == USER_TYPE_FIGHTER) {

			if ($user['purchase_status'] == NOT_PURCHASED) {
				$response['status'] = 403;
				$response['message'] = "You have not subscribed for this service";

				echoResponse(200, $response);
				return;
			}

			$fighter = extractVariables($requiredParams, $requestParams['event_fighter']);

			$figh = $notOrm->fighter()->where('user_id', $user['id'])->fetch();
			$fighter['fighter_id'] = $figh['id'];
			$prevParticipant = $notOrm->event_fighter()->where($fighter)->fetch();
			$result;

			if ($prevParticipant) {
				// $result = $prevParticipant;
				$result = iterator_to_array($prevParticipant);
			} else {
				$fighter['updated_time'] = date("Y-m-d H:i:s");
				$result = insertEventFighter($notOrm, $fighter);
				$result = iterator_to_array($result);
			}

			$response['message'] = "Event Fighter added";
			$response['status'] = 200;
			$response['event_fighter'] = $result;
			echoResponse(200, $response);
		} else {
			$response = "Error adding fighter";
			echoResponse(200, composeErrorResponse(400, $response));
		}
	} else {
		$message = "Invalid Request";
		if (!$credentialPresent) {
			$message = "Invalid Credentials";
		} else if (!$eventParamPresent) {
			$message = getErrorMsg($requiredParams, $requestParams['event_fighter']);
		}
		echoResponse(200, composeErrorResponse(400, $response));
	}
}

function getEventFighters() {
	global $app;
	global $notOrm;
	global $pdo;

	$app->contentType('application/json');
	$body = $app->request->getBody();
	$requestParams = json_decode($body, true);

//    $requiredParams = array("event_id", "user_agent");
	$credentialPresent = verifyRequiredCredentials($requestParams);
	$filterPresent = verifyRequiredParams(array('filter'), $requestParams);
	$eventParamPresent = FALSE;
	if ($filterPresent) {
		$eventParamPresent = verifyRequiredParams(array('event_id'), $requestParams['filter']) ||
		verifyRequiredParams(array('fighter_id'), $requestParams['filter']);
	} else {
		$message = "Filter missing";
		echoResponse(200, composeErrorResponse(400, $message));
		return;
	}

	if ($credentialPresent && $eventParamPresent) {
		$email = $requestParams['credential']['email'];
		$pass = $requestParams['credential']['pass'];

		/* $user = $notOrm->user()-> */
		/*                 select(implode(',', Constant::$user_projection))-> */
		/*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); */
		$userObject = new UserModel($notOrm);
		$user = $userObject->authenticateUser($email, $pass);
		if ($user) {

			if ($user['purchase_status'] == NOT_PURCHASED) {
				$response['status'] = 403;
				$response['message'] = "You have not subscribed for this service";

				echoResponse(200, $response);
				return;
			}
//            $filter = array_filter($requestParams['filter'], 'filterZeroes');
			$eventId = 0;
			if (isset($requestParams['filter']['event_id'])) {
				$eventId = $requestParams['filter']['event_id'];
			}

			/* $sql ="SELECT U.id,U.first_name,U.last_name,U.email,U.pass,U.user_type,U.gender,U.city,U.state, */
			/*     U.push_notification_id,U.user_agent,U.update_time,U.del_flag, */
			/*     F.id AS fid,F.user_id,F.weight_class_id,F.fight_team,F.update_time,F.mma,F.boxing,F.kickboxing,F.level, */
			/*     R.id AS rid,R.fighter_id,R.fight_type_id,R.draw,R.win,R.loss FROM user AS U LEFT JOIN fighter AS F ON U.id=F.user_id LEFT JOIN record AS R ON F.id = R.fighter_id */
			/*     WHERE F.id in (SELECT fighter_id FROM `event_fighter` WHERE event_id = :event_id)"; */

			$sql = "SELECT U.id,U.first_name,U.last_name,U.email,U.pass,U.user_type,U.gender,U.city,U.state,
			             U.push_notification_id,U.user_agent,U.update_time,U.del_flag,
			             F.id AS fid,F.user_id,F.weight_class_id,F.fight_team,F.update_time,F.mma,F.boxing,F.kickboxing,F.level,
			             R.id AS rid,R.fighter_id,R.fight_type_id,R.draw,R.win,R.loss,
			             C.name AS city_name, S.name AS state_name, WC.name AS weight_class_name
			             FROM user AS U LEFT JOIN city AS C on U.city = C.id LEFT JOIN state AS S on U.state = S.id  LEFT JOIN fighter AS F ON U.id=F.user_id
			             LEFT JOIN record AS R ON F.id = R.fighter_id LEFT JOIN weight_class AS WC ON WC.id = F.weight_class_id
			             WHERE F.id in (SELECT fighter_id FROM `event_fighter` WHERE event_id = :event_id)";

			$statement = $pdo->prepare($sql);
			$queryParams = array(':event_id' => $requestParams['filter']['event_id']);

			$statement->execute($queryParams);

			$fighters['event_fighters'] = array();

			$resultSet = $statement->fetchAll();

			$temp = null;
			foreach ($resultSet as $result) {
				/* echo 'result'; */
				if (is_null($temp) || $result['id'] != $temp['id']) {
					/* echo 'temp is null or id are same'; */
					if (!is_null($temp)) {
						/* echo 'id are not same. saving previous entry'; */
						$fighters['event_fighters'][] = $temp;
					}
					$temp = array(
						'id' => $result['id'],
						'first_name' => $result['first_name'],
						'last_name' => $result['last_name'],
						'email' => $result['email'],
						'user_type' => $result['user_type'],
						'gender' => $result['gender'],
						'city' => $result['city'],
						'state' => $result['state'],
						'type_info' => array(
							'weight_class_id' => $result['weight_class_id'],
							'weight_class_name' => $result['weight_class_name'],
							'fight_team' => $result['fight_team'],
							'level' => $result['level'],
							'mma' => $result['mma'],
							'boxing' => $result['boxing'],
							'kickboxing' => $result['kickboxing'],
							'record' => array(),

						),
					);

					if (!is_null($result['fighter_id'])) {
						$temp['type_info']['record'][] = array(
							'id' => $result['rid'],
							'fight_type_id' => $result['fight_type_id'],
							'fighter_id' => $result['fighter_id'],
							'win' => $result['win'],
							'loss' => $result['loss'],
							'draw' => $result['draw'],
						);
					}
				} else {

					if (!is_null($result['fighter_id'])) {
						/* echo 'id are same appending to record'; */
						$temp['type_info']['record'][] = array(
							'id' => $result['rid'],
							'fight_type_id' => $result['fight_type_id'],
							'fighter_id' => $result['fighter_id'],
							'win' => $result['win'],
							'loss' => $result['loss'],
							'draw' => $result['draw'],
						);
					}
				}
				/* echo 'First Name: '.$result['first_name']; */
			}
			if (!is_null($temp)) {
				$fighters['event_fighters'][] = $temp;
			}
			echoResponse(200, $fighters);
		} else {
			$response = "Invalid Credentials";
			echoResponse(200, composeErrorResponse(400, $response));
		}
	}
}

function removeEventFighter() {
	global $app;
	global $notOrm;

	$app->contentType('application/json');
	$body = $app->request->getBody();
	$requestParams = json_decode($body, true);

	$requiredParams = array("event_id");
	$credentialPresent = verifyRequiredCredentials($requestParams);
	$eventParamPresent = verifyRequiredParams(array('event_fighter'), $requestParams);
	$fighterParamPresent = FALSE;

	if ($eventParamPresent) {
		$fighterParamPresent = verifyRequiredParams($requiredParams, $requestParams['event_fighter']);
	} else {
		$response = "Invalid params";
		echoResponse(200, composeErrorResponse(400, $response));
		return;
	}

	if ($eventParamPresent && $credentialPresent) {
		$email = $requestParams['credential']['email'];
		$pass = $requestParams['credential']['pass'];

		/* $user = $notOrm->user()-> */
		/*                 select(implode(',', Constant::$user_projection))-> */
		/*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); */
		$userObject = new UserModel($notOrm);
		$user = $userObject->authenticateUser($email, $pass);

		if ($user && $user['user_type'] == USER_TYPE_FIGHTER) {

			if ($user['purchase_status'] == NOT_PURCHASED) {
				$response['status'] = 403;
				$response['message'] = "You have not subscribed for this service";

				echoResponse(200, $response);
				return;
			}
			$fighter = extractVariables($requiredParams, $requestParams['event_fighter']);
			$figh = $notOrm->fighter()->where('user_id', $user['id'])->fetch();
			$fighter['fighter_id'] = $figh['id'];
			// $fighter['fighter_id'] = $user['id'];
			$prevParticipant = $notOrm->event_fighter()->where($fighter)->fetch();
			$result;

			if ($prevParticipant) {
				$result = deleteEventFighter($notOrm, $fighter);
				// $result = $prevParticipant;
				// $response['event_fighter'] = $result;
				$response['message'] = "Event Fighter Removed";
				$response['status'] = 200;
			} else {
				// $result = insertEventFighter($notOrm, $fighter);
				$response['message'] = "You have not participated in this event";
				$response['status'] = 400;
			}

			echoResponse(200, $response);
		} else {
			$response = "Error adding fighter";
			echoResponse(200, composeErrorResponse(400, $response));
		}
	} else {
		$message = "Invalid Request";
		if (!$credentialPresent) {
			$message = "Invalid Credentials";
		} else if (!$eventParamPresent) {
			$message = getErrorMsg($requiredParams, $requestParams['event_fighter']);
		}
		echoResponse(400, composeErrorResponse(400, $response));
	}
}

function hasFighterParticipated() {
	global $app;
	global $notOrm;

	$app->contentType('application/json');
	$body = $app->request->getBody();
	$requestParams = json_decode($body, true);

//    $requiredParams = array("event_id", "user_agent");
	$credentialPresent = verifyRequiredCredentials($requestParams);
	$filterPresent = verifyRequiredParams(array('filter'), $requestParams);
	$eventParamPresent = FALSE;
	if ($filterPresent) {
		$eventParamPresent = verifyRequiredParams(array('event_id'), $requestParams['filter']);
	} else {
		$response = "Invalid params";
		echoResponse(200, composeErrorResponse(400, $response));
		return;
	}

	if ($credentialPresent) {
		$email = $requestParams['credential']['email'];
		$pass = $requestParams['credential']['pass'];
		$userObject = new UserModel($notOrm);
		$user = $userObject->authenticateUser($email, $pass);

		/* $user = $notOrm->user()-> */
		/*                 select(implode(',', Constant::$user_projection))-> */
		/*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); */
		if ($user && $eventParamPresent) {

			if ($user['purchase_status'] == NOT_PURCHASED) {
				$response['status'] = 403;
				$response['message'] = "You have not subscribed for this service";

				echoResponse(200, $response);
				return;
			}
			if (isset($requestParams['filter']['fighter_id']) && $requestParams['filter']['fighter_id'] > 0) {
				$fighterId = $requestParams['filter']['fighter_id'];
				$eventFighter = $notOrm->event_fighter()->where('fighter_id = ? AND event_id = ?', $fighterId, $requestParams['filter']['event_id'])->fetch();

				if ($eventFighter) {
					$response['has_participated'] = true;
					echoResponse(200, $response);
				} else {
					$response['has_participated'] = false;
					echoResponse(200, $response);
				}

			} else if ($user['user_type'] == USER_TYPE_FIGHTER) {
				$fighter = $notOrm->fighter()->where('user_id', $user['id'])->fetch();
				if ($fighter) {

					$eventFighter = $notOrm->event_fighter()->where('fighter_id = ? AND event_id = ?', $fighter['id'], $requestParams['filter']['event_id'])->fetch();
					if ($eventFighter) {
						$response['has_participated'] = true;
						echoResponse(200, $response);
					} else {
						$response['has_participated'] = false;
						echoResponse(200, $response);
					}

				} else {
					$response = "Fighter doesn't exist";
					echoResponse(200, composeErrorResponse(400, $response));
					return;
				}
			} else {
				$response = "Invalid fighter";
				echoResponse(200, composeErrorResponse(400, $response));
				return;
			}
		} else {
			$response = "Invalid credential";
			echoResponse(200, composeErrorResponse(400, $response));
			return;
		}
	}

}

function insertEventFighter($db, $fighterParams) {
	$fighterNotification['event_id'] = $fighterParams['event_id'];
	$fighterNotification['fighter_id'] = $fighterParams['fighter_id'];
	$db->fighter_notification()->insert($fighterNotification);
	return $db->event_fighter()->insert($fighterParams);
}

function deleteEventFighter($db, $fighter) {
	return $db->event_fighter()->where('fighter_id', $fighter['fighter_id'])->fetch()->delete();
}
