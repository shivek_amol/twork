<?php

/**
 * @apiDefine Event
 * @apiSuccess {Object} event Event
 * @apiSuccess {String} event.email email id
 * @apisuccess {number} event.id
 * @apiSuccess {number} event.user_id
 * @apiSuccess {string} event.event_name
 * @apiSuccess {string} event.event_date
 * @apiSuccess {number} event.event_weight_class_id
 * @apiSuccess {string} event.event_fight_type
 * @apiSuccess {string} event.event_address
 * @apiSuccess {number} event.event_city
 * @apiSuccess {number} event.event_state
 * @apiSuccess {string} event.contact_name
 * @apiSuccess {string} event.contact_email
 * @apiSuccess {string} event.contact_phone
 * @apiSuccess {string} event.phone_display_flag
 * @apiSuccess {string} event.update_time
 * @apiSuccess {string} event.del_flag,
 * @apiSuccess {string} event.user_agent
 */


/**
 * @apiDefine EventParam
 * @apiParam {Object} event Event
 * @apiParam {String} event.email email id
 * @apiParam {number} event.id
 * @apiParam {number} event.user_id
 * @apiParam {string} event.event_name
 * @apiParam {string} event.event_date
 * @apiParam {number} event.event_weight_class_id
 * @apiParam {string} event.event_fight_type
 * @apiParam {string} event.event_address
 * @apiParam {number} event.event_city
 * @apiParam {number} event.event_state
 * @apiParam {string} event.contact_name
 * @apiParam {string} event.contact_email
 * @apiParam {string} event.contact_phone
 * @apiParam {string} event.phone_display_flag
 * @apiParam {string} event.update_time
 * @apiParam {string} event.del_flag,
 * @apiParam {string} event.user_agent
 */


require_once 'include/Constant.php';
require_once 'include/Utils.php';
require_once 'Models/UserModel.php';



/**
 * @api {post} /post_event Post Event
 * @apiName PostEvent
 * @apiGroup Event
 *
 * @apiUse Credential
 * @apiUse EventParam
 *
 * @apiUse Event
 * @apiSuccess {string} message
 * @apiSuccess {number} status
 *
 */
$app->post("/post_event", "post_event");


/**
 * @api {post} /get_event Post Event
 * @apiName GetEvent
 * @apiGroup Event
 *
 * @apiUse Credential
 * @apiParam {Object} filter
 * @apiParam {String} filter.event_name
 * @apiParam {String} filter.event_date_from
 * @apiParam {String} filter.event_date_to
 * @apiParam {Number} filter.event_city
 * @apiParam {Number} filter.event_state
 * @apiParam {Number} filter.event_weight_class_id
 * @apiParam {Number} filter.event_fight_type
 *
 * @apiUse Event
 * @apiSuccess {string} message
 * @apiSuccess {number} status
 *
 */
$app->post("/get_event", "get_event");


/**
 * @api {post} /event Event Details
 * @apiName GetEventDetail
 * @apiGroup Event
 *
 * @apiUse Credential
 * @apiParam {Object} filter
 * @apiParam {Number} filter.event_id
 *
 * @apiUse Event
 *
 */
$app->post("/event", "getEventDetail");


/**
 * @api {post} /my_event Get My Event
 * @apiName MyEvent
 * @apiGroup Event
 *
 * @apiUse Credential
 *
 * @apiUse Event
 *
 */
$app->post("/my_event","getMyEvent");

global $logger;

function post_event() {
    global $logger;
    global $app;
    global $notOrm;
    $logger->info("post_event");
    $app->contentType('application/json');
    $body = $app->request->getBody();

    $requestParams = json_decode($body, true);


    $credentialPresent = verifyRequiredCredentials($requestParams);

    $eventParamsPresent = verifyRequiredParams(Constant::$event_insertion_params, $requestParams['event']);
   
   

    if ($credentialPresent && $eventParamsPresent) {
        $logger->info("credentialPresent and eventParamsPresent");
        $email = $requestParams['credential']['email'];
        $pass = $requestParams['credential']['pass'];
        /* $user = $notOrm->user()-> */
        /*                 select(implode(',', Constant::$user_projection))-> */
        /*                 where("email = ? AND pass = ?", $email, $pass)->fetch(); */

        $userObject = new UserModel($notOrm);
        $user = $userObject->authenticateUser($email , $pass);
         
        if ($user['user_type'] == USER_TYPE_PROMOTER) {
            $logger->info("user is promoter");
            $event = extractVariables(Constant::$event_insertion_params, $requestParams['event']);
            $event['user_id'] = $user['id'];
            $event['update_time'] = date('Y-m-d H:i:s');
            $row = $notOrm->event()->insert($event);
            $eventNot = array('event_id' => $row['id']);
            $eventNotification = $notOrm->event_notification()->insert($eventNot);
            $row = iterator_to_array($row);
            $response['status'] = 200;
            $response['message'] = 'Event saved successfully.';
            $response['event_id'] = $row;
            echoResponse(200, $response);
        } else {
            $logger->info("user is not promoter cannot add event.");
            //not permitted
            $response['status'] = 400;
            $response['message'] = 'Error adding event.';
            echoResponse(200, $response);
        }
    } else {
        $logger->info("credentialPresent and eventParamsPresent both false");
        $response['status'] = 400;
        $response['message'] = 'Invalid request';
        echoResponse(200, $response);
    }
}

function get_event() {


    global $app;
    global $notOrm;
    global $pdo;
    global $logger;

    $app->contentType('application/json');
    $body = $app->request->getBody();


    $user_for_event = json_decode($body, true);


    $response = array();
    $userObject = new UserModel($notOrm);
    $user = $userObject->authenticateUser($user_for_event['credential']['email'], 
        $user_for_event['credential']['pass']);
    /* $user = $notOrm->user()->where("email = ? AND pass = ?", $user_for_event['credential']['email'], md5($user_for_event['credential']['pass'])); */

//    $filters_enabled = isset($user_for_event['filter']); 
//    if ($user && $filters_enabled){
    // $events = array();
//          "filter":{
//  }   
    $user_filter = $user_for_event['filter'];
    $filter = array();
    $filter_provided = false;
    foreach ($user_filter as $key => $value) {
        $logger->info("User filter key: ".$key. " value: ".$value);
        if ((is_int($value) && $value > 0) || (!is_int($value) && strlen($value) > 0)) {
            $filter[$key] = $value;
            $filter_provided = true;
        }
    }
    if(isset($filter['event_city']) && $filter['event_city'] == 0){
        $logger->info("event city is zero unsetting");
        unset($filter['event_city']);
    }

    if(isset($filter['event_state']) && $filter['event_state'] == 0){
        $logger->info("event state is zero unsetting");
        unset($filter['event_state']);
    }

    if(isset($filter['event_weight_class_id']) && $filter['event_weight_class_id'] == 0){
        unset($filter['event_weight_class_id']);
    }

    if(isset($filter['event_fight_type']) && $filter['event_fight_type'] == 0){
        unset($filter['event_fight_type']);
    }
    /* if ($filter_provided && $user) { */
    if ($user) {

        if($user['purchase_status'] == NOT_PURCHASED && $user['user_type'] != USER_TYPE_VIEWER){
            $response['status'] = 403;
            $response['message'] = "You have not subscribed for this service";
            $response['events'][] = array();
            echoResponse(200, $response);
            return;
        }

        $sql = "SELECT E.id,E.user_id,E.event_name,E.event_date,E.event_weight_class_id,E.event_fight_type,E.event_address,E.event_city,E.event_state,E.contact_name,E.contact_email,
            E.contact_phone,E.phone_display_flag,E.update_time,E.del_flag,E.user_agent, C.name AS event_city_name, S.name AS event_state_name , WC.name AS event_weight_class_name 
            FROM event AS E LEFT JOIN city AS C ON E.event_city = C.id LEFT JOIN state AS S ON E.event_state = S.id LEFT JOIN weight_class AS WC ON E.event_weight_class_id = WC.id";


        $whereClause = "";
        $whereArgs = array();
        $firstQueryInitialized = false;
        if(isset($filter['event_name'])){
            $firstQueryInitialized = true;
            $whereClause = " E.event_name= :event_name";
            $whereArgs[':event_name'] = $filter['event_name'];
        }

//    "event_name":"event name like",
//    "event_date_from":"2014-12-04",
//    "event_date_to":"2014-12-04",
//    "event_city":"city"
//    "event_state":"state",
//    "event_weight_class_id":1,
//    "event_fight_type":1,
        $firstQueryInitialized = false;
        if(isset($filter['event_city'])){
            if($firstQueryInitialized){
                $whereClause = " AND ";
            }else{
                $firstQueryInitialized = true;
            }

            $whereClause .= " E.event_city= :event_city";
            $whereArgs[':event_city'] = $filter['event_city'];
        }

        if(isset($filter['event_state'])){
            if($firstQueryInitialized){
                $whereClause .= " AND ";
            }else{
                $firstQueryInitialized = true;
            }
            $whereClause .= " E.event_state= :event_state";
            $whereArgs[':event_state'] = $filter['event_state'];
        }

        if(isset($filter['event_weight_class_id'])){
            if($firstQueryInitialized){
                $whereClause .= " AND ";
            }else{
                $firstQueryInitialized = true;
            }
            $whereClause .= " E.event_weight_class_id= :event_weight_class_id";
            $whereArgs[':event_weight_class_id'] = $filter['event_weight_class_id'];
        }

        if(isset($filter['event_fight_type'])){
            if($firstQueryInitialized){
                $whereClause .= " and ";
            }else{
                $firstQueryInitialized = true;
            }
            $whereClause .= " E.event_fight_type= :event_fight_type";
            $whereArgs[':event_fight_type'] = $filter['event_fight_type'];
        }

        
        if(isset($filter['event_date_from']) && isset($filter['event_date_to'])){
            
            if($firstQueryInitialized){
                $whereClause .= " and ";
            }else{
                $firstQueryInitialized = true;
            }

            $whereClause .= " E.event_date BETWEEN :event_date_from AND :event_date_to ";
            $whereArgs[':event_date_from'] = $filter['event_date_from'];
            $whereArgs[':event_date_to'] = $filter['event_date_to'];


        }else if(isset($filter['event_date_from'])){

            if($firstQueryInitialized){
                $whereClause .= " and ";
            }else{
                $firstQueryInitialized = true;
            }

            $whereClause .= " E.event_date > :event_date_from";
            $whereArgs[':event_date_from'] = $filter['event_date_from'];
        }else if(isset($filter['event_date_to'])){

            if($firstQueryInitialized){
                $whereClause .= " and ";
            }else{
                $firstQueryInitialized = true;
            }

            $whereClause .= " E.event_date < :event_date_to ";
            $whereArgs[':event_date_to'] = $filter['event_date_to'];
        }

        if(strlen($whereClause)){
            $sql .= " WHERE ";
            $sql .= $whereClause;
        }

        $logger->info("final sql query: " . $sql);
        $stmt = $pdo->prepare($sql);
        $stmt->execute($whereArgs);
        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $events = array();
        foreach($resultSet as $result){
            $events[] = $result;
        }
        /* $events = array(); */
        /* foreach ($notOrm->event->where($filter) as $event) { */
        /*     $events[] = iterator_to_array($event); */
        /* } */

        $response = array("events" => array_values($events));
        $response['status'] = 200;
        $response['message'] = "Successful";
        echoResponse(200, $response);
    /* }  else if ($user) { */
        
    /*     if($user['purchase_status'] == NOT_PURCHASED && $user['user_type'] != USER_TYPE_VIEWER){ */
    /*         $response['status'] = 403; */
    /*         $response['message'] = "You have not subscribed for this service"; */
    /*         $response['events'] = array(); */
    /*         echoResponse(200, $response); */
    /*         return; */
    /*     } */
    /*     $events = array(); */
    /*     foreach ($notOrm->event() as $event) { */
    /*         $events[] = iterator_to_array($event); */
    /*     } */
    /*     $response = array("events" => array_values($events)); */
    /*     $response['status'] = 200; */
    /*     $response['message'] = "Successful"; */
    /*     echoResponse(200, $response); */
    } else {
        $response['status'] = 400;
        $response['message'] = 'Invalid Request';
        echoResponse(200, $response);
    }
}

function getMyEvent(){
    global $app;
    global $logger;
    global $notOrm;
    global $pdo;
    
    $app->contentType('application/json');
    $body = $app->request->getBody();
    $requestParams = json_decode($body, true);


    $credentialPresent = verifyRequiredCredentials($requestParams);

    if ($credentialPresent) {
        $email = $requestParams['credential']['email'];
        $pass = $requestParams['credential']['pass'];

        /* $user = $notOrm->user()-> */
        /*     select(implode(',', Constant::$user_projection))-> */
        /*     where("email = ? AND pass = ?", $email, $pass)->fetch(); */
        $userObject = new UserModel($notOrm);
        $user = $userObject->authenticateUser($email , $pass);
        if($user){
            $events;
            switch($user['user_type']){
            case USER_TYPE_VIEWER:
                $events = getUserEvent($pdo , $user);
                break;
            case USER_TYPE_FIGHTER:
                $fighter = $notOrm->fighter()->where('user_id',$user['id'])->fetch();
                $events = getFighterEvent($pdo , $fighter);
                break;
            case USER_TYPE_PROMOTER:
                $events = getPromoterEvent($pdo , $user);
                break;
            }
            $response['events'] = $events;
            echoResponse(200, $response);

        }else{
            $response['message'] = "Credentials invalid";
            $response['status'] = 400;
            echoResponse(200,$response);
        }
    }else {
        $response['message'] = "Invalid Request";
        $response['status'] = 400;
        echoResponse(200,$response);
    }


}

function getUserEvent($db, $user){
    $events = array();
    $sql = "SELECT E.id AS id,E.user_id,E.event_name,E.event_date,E.event_weight_class_id,
        E.event_fight_type,E.event_address,E.event_city,E.event_state,
        E.contact_name,E.contact_email,E.contact_phone , C.name AS event_city_name, S.name AS event_state_name , WC.name AS event_weight_class_name from event_participant as EP LEFT JOIN event AS E on EP.event_id = E.id LEFT JOIN city AS C ON E.event_city = C.id LEFT JOIN state AS S ON E.event_state = S.id LEFT JOIN weight_class AS WC ON E.event_weight_class_id = WC.id where EP.user_id = :user_id";
    $stmt = $db->prepare($sql);

    $stmt->execute(array(':user_id'=> $user['id']));
    $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach($resultSet as $result){
        $events[] = $result;
    }

    return $events;

}

function getFighterEvent($db, $fighter){
    $sql = "SELECT  E.id AS id,E.user_id,E.event_name,E.event_date,E.event_weight_class_id,
        E.event_fight_type,E.event_address,E.event_city,E.event_state,
        E.contact_name,E.contact_email,E.contact_phone , C.name AS event_city_name, S.name AS event_state_name , WC.name AS event_weight_class_name from event_fighter as EF LEFT JOIN event AS E on EF.event_id = E.id LEFT JOIN city AS C ON E.event_city = C.id LEFT JOIN state AS S ON E.event_state = S.id LEFT JOIN weight_class AS WC ON E.event_weight_class_id = WC.id where EF.fighter_id = :fighter_id";
    $stmt  = $db->prepare($sql);
    $stmt->execute(array(':fighter_id'=> $fighter['id']));
    $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $events = array();
    foreach($resultSet as $result){
        $events[] = $result;
    }
    return $events;
}

function getPromoterEvent($db, $user){
    /* $sql = "SELECT ".implode(',',Constant::$event_projection)." from event where user_id = :user_id"; */
    /* $sql = "SELECT * from event where user_id = :user_id"; */
        $sql = "SELECT E.id,E.user_id,E.event_name,E.event_date,E.event_weight_class_id,E.event_fight_type,E.event_address,E.event_city,E.event_state,E.contact_name,E.contact_email,
            E.contact_phone,E.phone_display_flag,E.update_time,E.del_flag,E.user_agent, C.name AS event_city_name, S.name AS event_state_name , WC.name AS event_weight_class_name 
            FROM event AS E LEFT JOIN city AS C ON E.event_city = C.id LEFT JOIN state AS S ON E.event_state = S.id LEFT JOIN weight_class AS WC ON E.event_weight_class_id = WC.id where E.user_id = :user_id";
    $stmt = $db->prepare($sql);
    $stmt->execute(array(':user_id'=> $user['id']));

    $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $events = array();
    foreach($resultSet as $result){
        /* var_dump($result); */
        $events[] = $result;
    }
    return $events;
}

function getEventDetail(){
    global $app;
    global $notOrm;

    $body = $app->request->getBody();
    $requestParams = json_decode($body, true);

    $credentialPresent = verifyRequiredCredentials($requestParams);
    $filterPresent = isset($requestParams['filter']['event_id']) && $requestParams['filter']['event_id'] > 0;
    if($credentialPresent && $filterPresent){ 
        $userObject = new UserModel($notOrm);
        $user = $userObject->authenticateUser($requestParams['credential']['email'], 
            $requestParams['credential']['pass']);
        /* $user_filter = $user_for_event['filter']; */
        if($user){
            $eventId = $requestParams['filter']['event_id'];
            $event = $notOrm->event->where('id' , $eventId)->fetch();
            if($event){
                $event = iterator_to_array($event);
                $response['event'] = $event;
                echoResponse(200, $response);
            }else{
                $response['status'] = 400;
                $response['message'] = 'Event doesnt exists';
                echoResponse(200, $response);
            }
        }else{
            //invalid user
            $response['status'] = 400;
            $response['message'] = 'Invalid Credentials';
            echoResponse(200, $response);
        }
    }else{
        // invalid params
        $response['status'] = 400;
        $response['message'] = 'Invalid Params';
        echoResponse(200, $response);
    }
}


?>
