<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function verifyRequiredCredentials($requestParams){
    if (isset($requestParams['credential'])){
        if (isset($requestParams['credential']['email']) && isset($requestParams['credential']['pass'])){
            return TRUE;
        }else{
            return FALSE;
        }
    }else{
        return FALSE;
    }
}


function verifyParams($requestParams){
    global $logger;

    $logger->info('verifyParams');

 $allowedRecordParams = array('win','loss','draw','fight_type_id');
    $allowedFighterParams = array('fight_team','mma','boxing','kickboxing','weight_class_id','level');
    $allowedPromoterParams = array('promotion_name');
    /* $userParamsPresent = verifyRequiredParams($allowedUserParams, $requestParams['user']); */
    $userParamsPresent = verifyUserParams($requestParams['user']);

    if($userParamsPresent){
        $logger->info('user params present');
        $userType = $requestParams['user']['user_type'];
        if($userType == USER_TYPE_FIGHTER){
            $logger->info('user is fighter');
            $fighterParamsPresent = verifyRequiredParams($allowedFighterParams,
                $requestParams['user']['type_info']);
            /* $fighterParamsPresent = $fighterParamsPresent && */ 
                 verifyRequiredParams($allowedRecordParams,$requestParams['user']['type_info']['record']); 
            if($fighterParamsPresent){
                $logger->info('fighter Params present');
                return true;
            }else{
                $logger->info('fighter params not present');
                return false;
            }

        }
        if($userType == USER_TYPE_PROMOTER){
            $promoterParamsPresent = verifyRequiredParams($allowedPromoterParams,
                $requestParams['user']['type_info']);
            if($promoterParamsPresent){
                return true;
            }else{
                return false;
            }
        }

        if($userType == USER_TYPE_VIEWER){
            return true;
        }
    }else{
        return false;
    }
}

function verifyUserParams($requestParams){

    $allowedUserParams = array('first_name','last_name','email','pass','user_type',
        'gender','city','gender','user_agent','push_notification_id');
    $userParamsPresent = verifyRequiredParams($allowedUserParams, $requestParams);
    return $userParamsPresent;
}

function verifyFighterParams($requestParams){
    $allowedRecordParams = array('win','loss','draw','fight_type_id');
    $allowedFighterParams = array('fight_team','mma','boxing','kickboxing','weight_class_id','level');

    $fighterParamsPresent = verifyRequiredParams($allowedFighterParams,
        $requestParams);
    return $fighterParamsPresent;
}

function verifyPromoterParams($requestParams){
    return true;
}

function verifyRequiredParams($required_fields , $user_params) {
    global $logger;

    $logger->info("verifyRequiredParams");
    $error = false;
    foreach ($required_fields as $field) {
        if (!isset($user_params[$field]) || (is_string($user_params[$field])&& strlen(trim($user_params[$field])) <= 0)) {
            $error = true;
            $logger->info($field.' not present');
            break;
        }
    }
    if ($error) {
        return FALSE;
    }else{
        $logger->info('All params present');
        return TRUE;
    }
    
}

function getErrorMsg($required_fields , $user_params){
        $error = false;
    $error_fields = "";

    foreach ($required_fields as $field) {
        if (!isset($user_params[$field]) || strlen(trim($user_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        return $response;
    }else{
        return TRUE;
    }
}

function extractVariables($required_fields, $params){
    $result = array();
    foreach ($required_fields as $field){
        $result[$field] = $params[$field];
    }
    return $result;
}

function composeErrorResponse($statusCode,$message){
    return array('status'=> $statusCode,'message'=>$message);
}

function filterEmpytString($var){
    if (is_string ( $var )){
        if (strlen($var) > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }else{
        return TRUE;
    }
}

function filterZeroes($var){
    if (is_int($var)){
        if($var > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }else{
        return FALSE;
    }
}
