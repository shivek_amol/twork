<?php

class Constant {
    
    public static $user_projection = array(
        "id", "first_name", "last_name", "email", "user_type", "gender",
        "city", "state","update_time");
    
    public static $event_projection = array(
        "id","user_id","event_name","event_date","event_weight_class_id",
        "event_fight_type","event_address","event_city","event_state",
        "contact_name","contact_email","contact_phone"
    );
    
     public static $event_insertion_params = array(
        "event_name","event_date","event_weight_class_id",
        "event_fight_type","event_address","event_city","event_state",
        "contact_name","contact_email","contact_phone","user_agent"
    );
   

    private static $status;

    static function getSuccess() {
        $status = 200;
        return $status;
    }

    static function getError() {
        $status = 404;
        return $status;
    }

    static function getUserName() {
        $status = 405;
        return $status;
    }

    static function getEmail_error() {
        $status = 406;
        return $status;
    }

    static function getProfileActive_error() {
        $status = 409;
        return $status;
    }

}

define('IS_USER', "1");
define('IS_FIGHTER', "2");
define('IS_PROMOTER', "3");



define('REG_IS_SUCC', 1);
define('REG_IS_EMAIL', 2);
define('REG_FIGHTER', 2);
define('REG_PROMOTER', 3);
define('REG_ERROR', 0);

define('LOG_SUCC', 1);
define('LOG_IS_FAILED', 0);

define('EVENTS_POST', 1);
define('EVENT_IS_NOT_POST', 0);
define('IS_NOT_PROMOTER', 2);

define('IS_NOT_AUTH', 5);

define('USER_TYPE_VIEWER', 1);
define('USER_TYPE_FIGHTER', 2);
define('USER_TYPE_PROMOTER',3);

define('FIGHT_TYPE_MMA',1);
define('FIGHT_TYPE_BOXING',2);
define('FIGHT_TYPE_KICKBOXING',3);

define('AMATURE_FIGHTER',1);
define('PROFESSIONAL_FIGHTER',2);


define('NOT_PURCHASED', 0);

?>
