<?php
error_reporting(0);
//ini_set("display_errors", "on");
//error_reporting(E_ALL);


/**
* Class to handle all db operations
* This class will have CRUD methods for database tables

*

* @author Amol

* @link URL Tutorial link

*/

class DbHandler {
  public $conn;

  function __construct() {

    //require_once dirname(__FILE__) . '/DbConnect.php';
    require_once 'DbConnect.php';
    require_once 'Constant.php';
    date_default_timezone_set("Asia/Kolkata");


    // opening db connection

    $db = new DbConnect();

    $this->conn = $db->connect();
    //        print_r($this->conn);

  }





  /* function start here*/

  function insert_user_data($data,$user_img)
  {
    $timenow= date('Y-m-d H:i:s');
    // $data = json_decode($data,true);
    global $final_id;  

    global $user_id;

// echo $user_img;
// print_r($data);

// echo $data['Name'];
    $is_employer = 0;
    
    if ($data['Role'] == 'employer') 
    {
      $is_employer = 1;
    }


    /* login detail start here */
    /* -------------------  Insert in login table  --------------------------*/


    

    // $originalDate = date_create('"'.$data['Dob'].'"');

    // $newDate = date_format($originalDate,"Y-m-d");


    $originalDate = new DateTime($data['Dob']);
    $newDate = $originalDate->format('Y-m-d');


    // $newDate = date("Y-m-d", strtotime($originalDate));


    $age =  date_diff(date_create($newDate), date_create('today'))->y;

  // $user_sql = "INSERT INTO ".LOGIN_DETAILS." VALUES(name,age,dob,Gender,picture,created_At)";


    $user_sql =  "INSERT INTO ".LOGIN_DETAILS." (name,age,dob,Gender,picture,IsEmployer,created_At,useraddress) 
    VALUES(?,?,?,?,?,?,?,?)";

 // echo "$user_sql";
    $stmt = $this->conn->prepare($user_sql);
    $stmt->bind_param("sisssiss",$data['Name'],$age,$newDate,$data['Gender'],$user_img,$is_employer,$timenow,$data['useraddres']);
      // $stmt->bind_param("ss",$data['Name'],$timenow);

    $stmt->execute();
    $user_id= $stmt->insert_id;

  // echo "  user_id=".$user_id;


    /* -------------------  Insertion end here   --------------------------*/


    /* -------------------  Insert profile image in image table --------------------------*/

    $profile_img_sql =  "INSERT INTO ".USER_PROFILE_IMAGE." (user_id,profile_image,created_By,created_At) VALUES(?,?,?,?)";

        // echo "$user_sql";
    $stmt = $this->conn->prepare($profile_img_sql);
    $stmt->bind_param("isis",$user_id,$user_img,$user_id,$timenow);
    $stmt->execute();
    $profile_iamge = $stmt->insert_id;  

    /* -------------------  Insertion end here ------------------ --------------------------*/

    /* -------------------  Update FB and GMAIL id here --------------------------*/


    if ($data['IdStatus'] == 'FBID')
    {

      $sql =  "UPDATE ".LOGIN_DETAILS." SET facebookId=?,created_By=?,updated_By=?,Updated_At=? WHERE UserId=? ";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("siisi", $data['Id'],$user_id,$user_id,$timenow,$user_id);
      $stmt->execute();
      $update= $stmt->affected_rows;


      
    }elseif ($data['IdStatus'] == 'GMAILID') {


      $sql =  "UPDATE ".LOGIN_DETAILS." SET googleId=?,created_By=?,updated_By=?,Updated_At=? WHERE UserId=? ";

      $stmt = $this->conn->prepare($sql);
      $stmt->bind_param("siisi", $data['Id'],$user_id,$user_id,$timenow,$user_id);
      $stmt->execute();
      $update= $stmt->affected_rows;


    }


    /* -------------------  Updatetion end here  --------------------------*/


    /* login table end here*/




    // if ($data['Role'] == 'Employer') 
    // {

    /*------------------------*/
    $active = 0;
    $emp_sql =  "INSERT INTO ".EMPLOYER." (user_id,active,created_By,created_At,lattitude,longitude,bio) VALUES(?,?,?,?,?,?,?)";
      // echo $emp_sql;

    $stmt = $this->conn->prepare($emp_sql);
    $stmt->bind_param("iiissss",$user_id,$active,$user_id,$timenow,$data['userlatitude'],$data['userlongitude'],$data['bio']);
    $stmt->execute();

    global $employer_id;  
    $employer_id = $stmt->insert_id;

      // echo "employer_id = $employer_id";

    /*----------------------------*/



    /*-----------------------------------*/
    global $job_id;


    if($is_employer == 1)
     {  // is employer



      $active = 1;
      $job_sql =  "INSERT INTO ".JOB_MASTER." (employer_id,job_descp,active,created_By,created_At,loc_name,loc_lat,loc_long,job_category,job_address) 
      VALUES(?,?,?,?,?,?,?,?,?,?)";
      // echo $job_sql;

      $stmt = $this->conn->prepare($job_sql);
      $stmt->bind_param("isiissssss",$employer_id,$data['Job_description'],$active,$user_id,$timenow,$data['JobLocation'],$data['userlatitude'],$data['userlongitude'],$data['CateName'],$data['useraddress']);
      $stmt->execute();
      $job_id = $stmt->insert_id;

      // echo "job_id = " . $job_id;

    }  // is employer end here


    /*--------------------------------------*/

// print_r($data['sub_cat_list']);



    // } else {

  /* employee entry start here */


  /*------------------------*/
  $active = 0;
  $emp_sql =  "INSERT INTO ".EMPLOYEE." (user_id,active,created_By,created_At,lattitude,longitude,bio) VALUES(?,?,?,?,?,?,?)";
      // echo $emp_sql;

  $stmt = $this->conn->prepare($emp_sql);
  $stmt->bind_param("iiissss",$user_id,$active,$user_id,$timenow,$data['userlatitude'],$data['userlongitude'],$data['bio']);
  $stmt->execute();

  global $employee_id;  
  $employee_id = $stmt->insert_id;

      // echo "employee_id = $employee_id";

  /*----------------------------*/





  /*------------- For Android setting  ----------------------*/
 
 // print_r($data['sub_cat_list']); 


  // foreach ($data['sub_cat_list']['sub_cat_list'][0] as $key => $sublist)
  

if (isset($data['sub_cat_list']['sub_cat_list'][0]))
 {




        // print_r($sublist);
       // echo $employer_id.",".$data['CateName'].",".$data['JobLocation'].",".$active.",".$user_id.",".$timenow;
        // $active = 1;





  foreach ($data['sub_cat_list']['sub_cat_list'][0] as $key => $sublist)
  {
          // echo $employer_id.",".$data['CateName'].",".$data['JobLocation'].",".$active.",".$user_id.",".$timenow;
        // $active = 1;



/*--------------------------------------------------------------------------*/



     if($is_employer == 1)
     {  // is employer
      $get_sub_id =  $this->conn->query("SELECT sub_Id FROM ".SUB_CATEGORY." WHERE subCat_Name = '".$sublist['subCat_Name']."'")->fetch_assoc();
      $job_sql =  "INSERT INTO ".R_JOB_SUBCATEGORY." (job_id,sub_id,wages,created_By,created_At) VALUES(?,?,?,?,?)";
         // echo $job_sql;
      $stmt = $this->conn->prepare($job_sql);
      $stmt->bind_param("iisis",$job_id,$get_sub_id['sub_Id'],$sublist['wages'],$user_id,$timenow);
      $stmt->execute();
      $final_id = $stmt->insert_id;
          // echo "job_sub_id = ".$final_id;
    }  // is employer end here




/*--------------------------------------------------------------------------*/


    $get_sub_id =  $this->conn->query("SELECT sub_Id FROM ".SUB_CATEGORY." WHERE subCat_Name = '".$sublist['subCat_Name']."'")->fetch_assoc();

    $job_sql =  "INSERT INTO ".R_EMPLOYEE_SUBCAT." (employee_Id,sub_id,wages,created_By,created_At) VALUES(?,?,?,?,?)";
        // echo $job_sql;

    $stmt = $this->conn->prepare($job_sql);
    $stmt->bind_param("iisis",$employee_id,$get_sub_id['sub_Id'],$sublist['wages'],$user_id,$timenow);
    $stmt->execute();
    $final_id = $stmt->insert_id;
        // echo "job_sub_id = ".$final_id;
      // } /* end of for each */
    /*--------------------------------------*/
    /* employee detail end here */
  }
  
} elseif(isset($data['sub_cat_list'])) 
{

  foreach ($data['sub_cat_list'] as $key => $sublist)
  {
      // echo $employer_id.",".$data['CateName'].",".$data['JobLocation'].",".$active.",".$user_id.",".$timenow;
        // $active = 1;




/*--------------------------------------------------------------------------*/



     if($is_employer == 1)
     {  // is employer
      $get_sub_id =  $this->conn->query("SELECT sub_Id FROM ".SUB_CATEGORY." WHERE subCat_Name = '".$sublist['subCat_Name']."'")->fetch_assoc();
      $job_sql =  "INSERT INTO ".R_JOB_SUBCATEGORY." (job_id,sub_id,wages,created_By,created_At) VALUES(?,?,?,?,?)";
         // echo $job_sql;
      $stmt = $this->conn->prepare($job_sql);
      $stmt->bind_param("iisis",$job_id,$get_sub_id['sub_Id'],$sublist['wages'],$user_id,$timenow);
      $stmt->execute();
      $final_id = $stmt->insert_id;
          // echo "job_sub_id = ".$final_id;
    }  // is employer end here




/*--------------------------------------------------------------------------*/


    

    $get_sub_id =  $this->conn->query("SELECT sub_Id FROM ".SUB_CATEGORY." WHERE subCat_Name = '".$sublist['subCat_Name']."'")->fetch_assoc();

    $job_sql =  "INSERT INTO ".R_EMPLOYEE_SUBCAT." (employee_Id,sub_id,wages,created_By,created_At) VALUES(?,?,?,?,?)";
        // echo $job_sql;

    $stmt = $this->conn->prepare($job_sql);
    $stmt->bind_param("iisis",$employee_id,$get_sub_id['sub_Id'],$sublist['wages'],$user_id,$timenow);
    $stmt->execute();
    $final_id = $stmt->insert_id;
        // echo "job_sub_id = ".$final_id;
      // } /* end of for each */
    /*--------------------------------------*/
    /* employee detail end here */
    }  // else end here
  
}




  

    $stmt->close();

    $user_detail['user_id'] = $user_id;
    $user_detail['employer_id'] = $employer_id;
    $user_detail['employee_id'] = $employee_id;
    return $user_detail;

  }


  /* function end here*/























  /* function start here */

  function getUserProfileData($data)
  {
    // print_r($data);
    $sql = "SELECT * FROM ".LOGIN_DETAILS." WHERE UserId = '".$data['user_id']."'";
    $stmt = $this->conn->query($sql);
    $user_detail = array();

// echo $sql;


    global $key_array;




    if ($row = $stmt->fetch_assoc()) 
    {

    // print_r($row);



          // $IsEmp =  $row['IsEmployer'];
          /*
          1 = Employer
          0 =  Employee
          */

          if ($row['IsEmployer'] == 1)
          {

            $num = 0;


            $key_array = array();




            /* ======    get employee list       ==========*/
            /* get employer_id */  
            $emplr_sql = "SELECT employer_Id FROM ".EMPLOYER." WHERE user_id ='".$row['UserId']."'";
              // echo $emplr_sql;
            $emplr_stmt = $this->conn->query($emplr_sql)->fetch_assoc();
              // print_r($emplr_stmt);
              // echo "employer_id = " .$emplr_stmt['employer_Id'] ;
            $joinsql ="SELECT r_js.sub_id,jm.loc_name,jm.job_descp,jm.job_status_id,jm.job_Id,jm.loc_lat,jm.loc_long FROM ".JOB_MASTER." AS jm INNER JOIN ".R_JOB_SUBCATEGORY." AS r_js ON jm.job_Id=r_js.job_id WHERE jm.employer_id='".$emplr_stmt['employer_Id']."'";
      // echo $joinsql;
            $jm_stmt = $this->conn->query($joinsql);
            $index = 0;
            $flag = 0;
            $result_array = array();
            while ( $list = $jm_stmt->fetch_assoc())
            {

              // print_r($list);

              $emp_sql = "SELECT employee_Id,wages FROM ".R_EMPLOYEE_SUBCAT." WHERE sub_id = '".$list['sub_id']."'";
              $getemployee_id = $this->conn->query($emp_sql);
              while( $get_id =$getemployee_id->fetch_assoc()) 
              {
                $emp_id = $get_id['employee_Id'];
                    // echo "emp = $emp_id";
                $sql ="SELECT * FROM ".EMPLOYEE." AS emp INNER JOIN ".LOGIN_DETAILS." AS ld ON emp.user_id = ld.UserId WHERE emp.emp_Id ='".$emp_id."' AND ld.IsEmployer != 1";
                    // echo $sql;

                $stmt = $this->conn->query($sql);

                if ($row = $stmt->fetch_assoc()) 
                {


                  if (array_key_exists ($emp_id , $result_array ))
                  {
                        // echo "present ";
                   $query = "SELECT sub_cat.subCat_Name,sub_cat.sub_cat_img,cat.cat_name FROM ".SUB_CATEGORY." AS sub_cat INNER JOIN ".CATEGORY." AS cat ON sub_cat.cat_id=cat.cat_Id WHERE sub_cat.sub_Id ='".$list['sub_id']."'";
                  // echo $query;
                   $sub_stmt = $this->conn->query($query);
                   if ($result = $sub_stmt->fetch_assoc()) 
                   {
                          // $user_detail[$index]['CateName'] = $result['cat_name'];
                    $result_array[$emp_id]['sub_cat'][$flag]['subCat_Name'] = $result['subCat_Name'];
                    $result_array[$emp_id]['sub_cat'][$flag]['sub_cat_img'] = $result['sub_cat_img'];
                    $result_array[$emp_id]['sub_cat'][$flag]['wages'] = $get_id['wages'];

                   } // end of if

                   $sub_stmt->close();



                 } else {
                   

                   $eplr_sql = "SELECT employer_Id FROM ".EMPLOYER." WHERE user_id ='".$row['UserId']."'";
              // echo $emplr_sql;
                   $eplr_stmt = $this->conn->query($eplr_sql)->fetch_assoc();


                   $user_detail['UserId'] = $row['UserId'];
                   $user_detail['employee_Id'] = $emp_id;
                   $user_detail['employer_Id'] = $eplr_stmt['employer_Id'];


                   $user_detail['name'] = $row['name'];
                   $user_detail['age'] = $row['age'];
                   $user_detail['picture'] =$row['picture'];

                   $user_detail['Gender'] = $row['Gender'];
                   $user_detail['bio'] = $row['bio'];
                   $user_detail['useraddress'] = $row['useraddress'];
                   $user_detail['lattitude'] = $row['lattitude'];
                   $user_detail['longitude'] = $row['longitude'];
                   
                   /* job related feilds */


                   $user_detail['job_Id'] = $list['job_Id'];

                   $user_detail['job_descp'] = $list['job_descp'];
                   $user_detail['loc_name'] = $list['loc_name'];
                  

                   $user_detail['job_status_id'] = $list['job_status_id'];
                  // $user_detail['wages'] = $get_id['wages']; 

                   $user_detail['distance'] =$this->distance($list['loc_lat'],$list['loc_long'], $row['lattitude'],$row['longitude'], "K") . "km";
                   



                  // $user_detail['active'] =$row['active'];

                  // $eplr_stmt->close();


                   $query = "SELECT sub_cat.subCat_Name,sub_cat.sub_cat_img,cat.cat_name FROM ".SUB_CATEGORY." AS sub_cat INNER JOIN ".CATEGORY." AS cat ON sub_cat.cat_id=cat.cat_Id WHERE sub_cat.sub_Id ='".$list['sub_id']."'";
                  // echo $query;
                   $sub_stmt = $this->conn->query($query);
                   if ($result = $sub_stmt->fetch_assoc()) 
                   {
                          // $user_detail[$index]['CateName'] = $result['cat_name'];
                    $user_detail['CateName'] = $result['cat_name'];
                    $user_detail['sub_cat'][$flag]['subCat_Name'] = $result['subCat_Name'];
                    $user_detail['sub_cat'][$flag]['sub_cat_img'] = $result['sub_cat_img'];
                    $user_detail['sub_cat'][$flag]['wages'] = $get_id['wages'];
                   } // end of if

                   $sub_stmt->close();

                   $result_array[$emp_id]= $user_detail;

                   $key_array[$num] = $emp_id;
                   $num =  $num+1;
                 }

                 } // emp if  end here 


                 $stmt->close();

                // echo $list['sub_id'];


                 $index++;  

                  // echo $index;

                } // end while here 


                $getemployee_id->close();

                $flag++;

      } // end of while


    //           /* ======    if end here       ==========*/
                  // print_r($user_detail);

    } else {

      /* ======    get employer list       ==========*/

      $num = 0;


      $key_array = array();


      $emplr_sql = "SELECT emp_Id FROM ".EMPLOYEE." WHERE user_id ='".$row['UserId']."'";
              // echo $emplr_sql;
      $emplr_stmt = $this->conn->query($emplr_sql)->fetch_assoc();
              // print_r($emplr_stmt);
              // echo "employer_id = " .$emplr_stmt['employer_Id'] ;


      $joinsql ="SELECT r_js.sub_id FROM ".R_EMPLOYEE_SUBCAT." AS r_js WHERE r_js.employee_Id='".$emplr_stmt['emp_Id']."'";

      // echo $joinsql;

      $jm_stmt = $this->conn->query($joinsql);

      $index = 0;
      $flag = 0;
      $result_array = array();

      while ( $list = $jm_stmt->fetch_assoc())
      {

        $emp_sql = "SELECT jm.employer_id,jm.job_descp,jm.loc_name,jm.job_status_id,r_j_sub.wages,jm.job_Id,jm.loc_lat,jm.loc_long FROM ".R_JOB_SUBCATEGORY." AS r_j_sub INNER JOIN ".JOB_MASTER." AS jm ON jm.job_Id=r_j_sub.job_id WHERE r_j_sub.sub_id = '".$list['sub_id']."'";



        $getemployee_id = $this->conn->query($emp_sql);


        while( $get_id =$getemployee_id->fetch_assoc()) 
        {
          $emp_id = $get_id['employer_id'];



                    // echo "emp = $emp_id";

          $sql ="SELECT * FROM ".EMPLOYER." AS emp INNER JOIN ".LOGIN_DETAILS." AS ld ON emp.user_id = ld.UserId WHERE emp.employer_Id ='".$emp_id."' AND ld.IsEmployer != 0";
                    // echo $sql;

          $stmt = $this->conn->query($sql);

          if ($row = $stmt->fetch_assoc()) 
          {


            if (array_key_exists ($emp_id , $result_array ))
            {



                        // echo "present ";
             $query = "SELECT sub_cat.subCat_Name,sub_cat.sub_cat_img,cat.cat_name FROM ".SUB_CATEGORY." AS sub_cat INNER JOIN ".CATEGORY." AS cat ON sub_cat.cat_id=cat.cat_Id WHERE sub_cat.sub_Id ='".$list['sub_id']."'";
                  // echo $query;
             $sub_stmt = $this->conn->query($query);
             if ($result = $sub_stmt->fetch_assoc()) 
             {
                          // $user_detail[$index]['CateName'] = $result['cat_name'];


              $result_array[$emp_id]['sub_cat'][$flag]['subCat_Name'] = $result['subCat_Name'];
              $result_array[$emp_id]['sub_cat'][$flag]['sub_cat_img'] = $result['sub_cat_img'];
              $result_array[$emp_id]['sub_cat'][$flag]['wages'] = $get_id['wages'];

                   } // end of if

                   $sub_stmt->close();



                 } else {

                  $eplr_sql = "SELECT * FROM ".EMPLOYEE." WHERE user_id ='".$row['UserId']."'";
              // echo $emplr_sql;
                  $eplr_stmt = $this->conn->query($eplr_sql)->fetch_assoc();




                  $user_detail['UserId'] = $row['UserId'];
                  $user_detail['employee_Id'] = $eplr_stmt['emp_Id'];
                  

                  $user_detail['employer_Id'] = $row['employer_Id'];



                  $user_detail['name'] = $row['name'];
                  $user_detail['age'] = $row['age'];
                  $user_detail['picture'] =$row['picture'];

                  $user_detail['Gender'] = $row['Gender'];
                  $user_detail['bio'] = $row['bio'];

                     $user_detail['useraddress'] = $row['useraddress'];
                   $user_detail['lattitude'] = $row['lattitude'];
                   $user_detail['longitude'] = $row['longitude'];



                  // $user_detail['active'] =$row['active'];

                  /* job related feilds */
                  $user_detail['job_Id'] = $get_id['job_Id'];
                  $user_detail['job_descp'] = $get_id['job_descp'];
                  $user_detail['loc_name'] = $get_id['loc_name'];
                  $user_detail['job_status_id'] = $get_id['job_status_id'];
                  // $user_detail['wages'] = $get_id['wages'];
                  

// echo $get_id['loc_lat'] .",". $get_id['loc_long'] .",". $row['lattitude'].",".$row['longitude'] ;  


                  $user_detail['distance'] = $this->distance($eplr_stmt['lattitude'],$eplr_stmt['longitude'],$get_id['loc_lat'],$get_id['loc_long'],"K")."km"; 

                    // $eplr_stmt->close();


                  $query = "SELECT sub_cat.subCat_Name,sub_cat.sub_cat_img,cat.cat_name FROM ".SUB_CATEGORY." AS sub_cat INNER JOIN ".CATEGORY." AS cat ON sub_cat.cat_id=cat.cat_Id WHERE sub_cat.sub_Id ='".$list['sub_id']."'";
                  // echo $query;
                  $sub_stmt = $this->conn->query($query);
                  if ($result = $sub_stmt->fetch_assoc()) 
                  {
                          // $user_detail[$index]['CateName'] = $result['cat_name'];
                    $user_detail['CateName'] = $result['cat_name'];


                    $user_detail['sub_cat'][$flag]['subCat_Name'] = $result['subCat_Name'];
                    $user_detail['sub_cat'][$flag]['sub_cat_img'] = $result['sub_cat_img'];
                    $user_detail['sub_cat'][$flag]['wages'] = $get_id['wages'];

                   } // end of if

                   $sub_stmt->close();



                   $result_array[$emp_id]= $user_detail;
                   
                   $key_array[$num] = $emp_id;
                   $num =  $num+1;

                 }

                 } // emp if  end here 


                 $stmt->close();

                // echo $list['sub_id'];


                 $index++;  

                  // echo $index;

                } // end while here 


                $getemployee_id->close();

                $flag++;

      } // end of while



      /* ======    else end here       ==========*/

    }





    } // end userid if 

                      // echo "count = ".count($result_array);

    // $result_array = array_unique($result_array);
// print_r($result);

    
    $result_array['key'] = $key_array; 




    foreach ($key_array as $key => $key_value) {
      $emp_sql =  "INSERT INTO ".USER_PROFILE_LIST_QUEUE." (user_id,job_id,key_value,profile_detail,is_paid) VALUES(?,?,?,?,?)";
      
      $is_paid = 0;
      $stmt = $this->conn->prepare($emp_sql);
      $stmt->bind_param("iissi",$data['user_id'],$result_array[$key_value]['job_Id'],$key_value,json_encode($result_array[$key_value]),$is_paid);
      $stmt->execute();
      $stmt->close();

    }

    $result_array = array();



    $get_sql = "SELECT * FROM ".USER_PROFILE_LIST_QUEUE." WHERE user_id = ".$data['user_id']." LIMIT 0 , 5";

    $index = 0 ;

    $sub_stmt = $this->conn->query($get_sql);
    while($result = $sub_stmt->fetch_assoc()) 
    {
      $result_array[$index] = json_decode($result['profile_detail'],true);
      $index++;

      $del_sql = "DELETE FROM ".USER_PROFILE_LIST_QUEUE." WHERE id = '".$result['id']."'";

 

      if (     $this->conn->query($del_sql) === TRUE) {
        // echo "Record deleted successfully";
      }

  } // end of if

  $sub_stmt->close();






/* for loop */
    return $result_array;
    $jm_stmt->close();
    $emplr_stmt->close();
  

  }


  /*  get distance function  start here */


  function distance($lat1, $lon1, $lat2, $lon2, $unit) {

    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
      return ($miles * 1.609344);
    } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
      return $miles;
    }
  }


  /* get distance functio end here */ 



















  /* function end here */


  function arraycount($val,$list){
    $yesval =0;
    $count_num = count($result_array);
    for ($ind=0; $ind < $count_num ; $ind++) { 
      
      if (array_search($val, $list[$ind][1])){
        
        echo "present = ".$list[$ind][1];
        $yesval = $ind;
  // break;
      } 
      
    }
// return $yesval;

  }






  // function used for to get all client table data .

  function get_client_table()
  {

    echo " Function calll";

    // $stmt = $this->conn->query("SELECT * FROM ".CLIENT_TABLE);
    // $index=0;
    // $client_data=array();
    // while($user_result_array = $stmt->fetch_assoc())
    // {
    //   $client_data[$index]['client_id'] = $user_result_array['client_id'];
    //   $client_data[$index]['name'] = $user_result_array['name'];
    //   $client_data[$index]['phone_no'] = $user_result_array['phone_no'];
    //   $client_data[$index]['address'] = $user_result_array['address'];
    //   $index++;
    // }
    // return $client_data;
  }// end function

  /* get category List */

  function get_category_list()
  {

   $stmt = $this->conn->query("SELECT * FROM ".CATEGORY);
   $index=0;
   $categorylist=array();
   while($user_result_array = $stmt->fetch_assoc())
   {
    $categorylist[$index]['cat_Id'] = $user_result_array['cat_Id'];
    $categorylist[$index]['cat_name'] = $user_result_array['cat_name'];
    $categorylist[$index]['cat_img_name'] = $user_result_array['img_name'];
    $index++;
  }

  $stmt->close();

  return $categorylist;

}


/* function end here */


/* function start here */

function get_sub_category_list($categoryJason)
{
  // echo "string";
  $categoryJason = json_decode($categoryJason,true);
  // print_r($categoryJason);

  $stmt = $this->conn->query("SELECT * FROM ".SUB_CATEGORY." WHERE cat_id = '".$categoryJason['cateId']."'" );
  $index=0;
  $sub_categorylist=array();
  while($user_result_array = $stmt->fetch_assoc())
  {
      // print_r($user_result_array);
    $sub_categorylist[$index]['sub_Id'] = $user_result_array['sub_Id'];
    $sub_categorylist[$index]['subCat_Name'] = $user_result_array['subCat_Name'];
    $sub_categorylist[$index]['sub_cat_img'] = $user_result_array['sub_cat_img'];

    $index++;
  }
  return $sub_categorylist;

}


/* function end here*/




} // end of class


?>
